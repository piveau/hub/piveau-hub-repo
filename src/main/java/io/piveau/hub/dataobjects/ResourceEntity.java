package io.piveau.hub.dataobjects;

import io.piveau.dcatap.DCATAPUriRef;
import io.piveau.dcatap.DCATAPUriSchema;
import io.piveau.profile.ExtraResource;
import io.piveau.rdf.Piveau;
import io.piveau.utils.JenaUtils;
import io.piveau.vocabularies.vocabulary.DEFECTIVE_SPDX;
import io.piveau.vocabularies.vocabulary.PV;
import io.piveau.vocabularies.vocabulary.SPDX;
import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ResIterator;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.shared.DoesNotExistException;
import org.apache.jena.vocabulary.DCAT;
import org.apache.jena.vocabulary.DCTerms;
import org.apache.jena.vocabulary.RDF;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

public class ResourceEntity {

    private static final Logger log = LoggerFactory.getLogger(ResourceEntity.class);

    private DCATAPUriRef catalogueUriRef;
    private String originId;

    private DCATAPUriRef uriSchema;

    private Model model;

    private final ExtraResource extraResource;
    private final String baseUri;

    private final String CLASS_URI= PV.CustomResource.getURI();


    public ResourceEntity(String content, String contentType, ExtraResource extraResource, String baseUri) {
        this.extraResource = extraResource;
        this.baseUri = baseUri;

        Model temp = Piveau.toModel(content.getBytes(), contentType, "");

        temp.listResourcesWithProperty(RDF.type, DCAT.CatalogRecord).toList()
                .forEach(record -> {
                    if (!record.hasProperty(DCTerms.creator, temp.createResource("http://piveau.io"))) {
                        Model recordModel = Piveau.extractAsModel(record);
                        temp.remove(recordModel);
                    }
                });

        temp.listResourcesWithProperty(RDF.type, CLASS_URI).nextOptional().ifPresentOrElse(res -> {
            model = temp;
            extractId();
        }, () -> {
            throw new DoesNotExistException(CLASS_URI);
        });
    }


    private void extractId() {
        ResIterator it = model.listSubjectsWithProperty(RDF.type, DCAT.CatalogRecord);
        if (it.hasNext()) {
            Resource record = it.next();
            if (record.isURIResource() && DCATAPUriSchema.isRecordUriRef(record.getURI()) && record.hasProperty(DCTerms.identifier)) {
                originId = record.getProperty(DCTerms.identifier).getLiteral().getLexicalForm();
            }
        }

        it = model.listSubjectsWithProperty(RDF.type, DCAT.Dataset);
        if (it.hasNext()) {
            Resource dataset = it.next();
            if (dataset.isURIResource() && DCATAPUriSchema.isDatasetUriRef(dataset.getURI())) {
                uriSchema = DCATAPUriSchema.parseUriRef(dataset.getURI());
            }
        }
    }


    private void updateRecord(Resource record, String hash) {
        // We could clean up defective spdx checksum here

        Resource checksum = record.getPropertyResourceValue(SPDX.checksum);
        if (checksum == null) {
            // repair
            checksum = record.getModel().createResource(SPDX.Checksum);
            checksum.addProperty(SPDX.algorithm, SPDX.checksumAlgorithm_md5);
            record.addProperty(SPDX.checksum, checksum);
        }
        // repair
        if (!checksum.hasProperty(RDF.type, SPDX.Checksum)) {
            checksum.addProperty(RDF.type, SPDX.Checksum);
        }
        // repair
        if (!checksum.hasProperty(SPDX.algorithm)) {
            checksum.addProperty(SPDX.algorithm, SPDX.checksumAlgorithm_md5);
        }
        checksum.removeAll(SPDX.checksumValue);
        checksum.addProperty(SPDX.checksumValue, hash);

        record.removeAll(DCTerms.modified);
        record.addProperty(DCTerms.modified, ZonedDateTime
                .now(ZoneOffset.UTC)
                .truncatedTo(ChronoUnit.SECONDS)
                .format(DateTimeFormatter.ISO_DATE_TIME), XSDDatatype.XSDdateTime);

        record.addProperty(DCTerms.creator, record.getModel().createResource("http://piveau.io"));
    }

    public String hash() {
        try {
            return JenaUtils.canonicalHash(model);
        } catch (Exception e) {
            log.error("Calculating hash failure", e);
            return "";
        }
    }
}
