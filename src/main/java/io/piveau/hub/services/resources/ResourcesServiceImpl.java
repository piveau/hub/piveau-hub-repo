/*
 * Copyright (c) Fraunhofer FOKUS
 *
 * This program and the accompanying materials are made available under the
 * terms of the Apache License, Version 2.0 which is available at
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package io.piveau.hub.services.resources;

import io.piveau.dcatap.*;
import io.piveau.hub.Constants;
import io.piveau.hub.Defaults;
import io.piveau.hub.services.index.IndexService;
import io.piveau.profile.ExtraResource;
import io.piveau.profile.Indexer;
import io.piveau.profile.PiveauProfile;
import io.piveau.rdf.Piveau;
import io.piveau.sparql.SparqlQueryPool;
import io.piveau.utils.JenaUtils;
import io.piveau.utils.PiveauContext;
import io.piveau.vocabularies.Concept;
import io.piveau.vocabularies.Languages;
import io.vertx.core.*;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.serviceproxy.ServiceException;
import kotlin.NotImplementedError;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.ResIterator;
import org.apache.jena.vocabulary.RDF;
import org.ehcache.Cache;
import org.ehcache.CacheManager;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.CacheManagerBuilder;
import org.ehcache.config.builders.ExpiryPolicyBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.config.units.EntryUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.file.Path;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class ResourcesServiceImpl implements ResourcesService {

    private final Logger log = LoggerFactory.getLogger(getClass());

    private final PiveauContext serviceContext;
    private final TripleStore tripleStore;
    private final CatalogueManager catalogueManager;
    private final ResourceManager resourceManager;


    private final IndexService indexService;

    private final String baseUri;

    private final PiveauProfile profile;
    private final SparqlQueryPool sparqlQueryPool;

    private final Cache<String, String> cache;

    ResourcesServiceImpl(Vertx vertx, TripleStore tripleStore, JsonObject config, PiveauProfile profile,
                         Handler<AsyncResult<ResourcesService>> handler) {
        this.tripleStore = tripleStore;
        catalogueManager = tripleStore.getCatalogueManager();
        resourceManager = tripleStore.getResourceManager();
        this.profile = profile;
        serviceContext = new PiveauContext("hub-repo", "ResourcesService");


        JsonObject indexConfig = config.getJsonObject(Constants.ENV_PIVEAU_HUB_SEARCH_SERVICE, new JsonObject());
        if (indexConfig.getBoolean("enabled", Defaults.SEARCH_SERVICE_ENABLED)) {
            indexService = IndexService.createProxy(vertx, IndexService.SERVICE_ADDRESS, IndexService.DEFAULT_TIMEOUT);
        } else {
            indexService = null;
        }

        try (CacheManager cacheManager = CacheManagerBuilder.newCacheManagerBuilder()
                .withCache("resourcesService", CacheConfigurationBuilder.newCacheConfigurationBuilder(String.class, String.class,
                                ResourcePoolsBuilder.newResourcePoolsBuilder().heap(100, EntryUnit.ENTRIES))
                        .withExpiry(ExpiryPolicyBuilder.timeToIdleExpiration(Duration.ofHours(12))))
                .build(true)) {

            cache = cacheManager.getCache("resourcesService", String.class, String.class);
        }
        sparqlQueryPool = SparqlQueryPool.Companion.create(vertx, Path.of("queries"));

        JsonObject schemaConfig = config.getJsonObject(Constants.ENV_PIVEAU_DCATAP_SCHEMA_CONFIG, new JsonObject());
        baseUri = schemaConfig.getString("baseUri", "https://piveau.io/") +
                schemaConfig.getString("resource", "set/resource/");

        handler.handle(Future.succeededFuture(this));
    }

    @Override
    public Future<JsonArray> listResourceTypes() {
        Promise<JsonArray> promise = Promise.promise();
        JsonArray jsonResult = new JsonArray();

        profile.extraResources.keySet().forEach(jsonResult::add);

        promise.complete(jsonResult);
        return promise.future();
    }

    @Override
    public Future<JsonArray> listResources(String type, int offset, int limit) {
        try {
            Promise<JsonArray> promise = Promise.promise();

            if (profile.extraResourceIsSet(type)) {
                ExtraResource resource = profile.getExtraResource(type);
                return resourceManager.list(resource.getTypeURI(), resource.isInCatalog(), offset, limit).map(JsonArray::new);

            } else {
                promise.fail(new ServiceException(404, "Resource type unknown"));
            }

            return promise.future();
        } catch (Exception e) {
            return Future.failedFuture(new ServiceException(500, "Internal Server Error"));
        }
    }

    /**
     * List all resources of a given type in a catalogue
     *
     * @param type        the resource type
     * @param catalogueId the catalogue id
     * @return a future that completes with a list of resources. If the catalogue is not found, the future will fail with a ServiceException
     */
    @Override
    public Future<JsonArray> listResourcesWithCatalogue(String type, String catalogueId, int offset, int limit) {
        try {
            if (catalogueId == null || catalogueId.isEmpty()) {
                return listResources(type, offset, limit);
            }

            Promise<JsonArray> promise = Promise.promise();

            if (profile.extraResourceIsSet(type)) {
                ExtraResource resource = profile.getExtraResource(type);

                //TODO: implement listing for catalogues
                throw new NotImplementedError("Catalogue is not implemented yet");

            } else {
                promise.fail(new ServiceException(404, "Resource type unknown"));
            }

            return promise.future();
        } catch (Exception e) {
            return Future.failedFuture(new ServiceException(500, "Internal Server Error"));
        }

    }



    /**
     * Get a resource of a given type in a catalogue
     *
     * @param id          the resource id
     * @param type        the resource type
     * @param catalogueId the catalogue id
     * @param acceptType  the accept type
     * @return a future that completes with the resource. If the resource is not found, the future will fail with a ServiceException
     */
    @Override
    public Future<String> getResource(String id, String type, String catalogueId, String acceptType) {
        String graphName = baseUri + type + "/" + id;


        return validateCatalog(type, catalogueId)
                .compose(r -> getSingleResource(graphName, acceptType));


    }


    private Future<String> getSingleResource(String uri, String acceptType) {

        return resourceManager.getGraph(uri).map(model -> {
            if (model.isEmpty()) {
                throw new ServiceException(404, "Resource not found");
            } else {
                Prefixes.setNsPrefixesFiltered(model);
                return Piveau.presentAs(model, acceptType);
            }
        });


    }




    /**
     * Delete a resource graph from the triplestore and index. No further checks are executed before
     *
     * @param id   the resource id
     * @param type the resource type
     * @return a future that completes when the resource is deleted. If the resource is not found, the future will fail with a ServiceException
     */
    private Future<Void> deleteSingleResource(String id, String type) {
        String graphName = baseUri + type + "/" + id;
        return tripleStore.deleteGraph(graphName)
                .compose(v -> indexService.deleteResource(id, type))
                .<Void>mapEmpty()
                .recover(cause -> {
                    if (cause.getMessage().contains("Not Found")) {
                        throw new ServiceException(404, "Resource not found");
                    } else {
                        throw new ServiceException(500, cause.getMessage());
                    }
                });
    }


    /**
     * Delete a resource that is part of a catalogue.
     * If the resource type requires a catalogue, this method will check if the catalogue is set and if the resource is part of the catalogue.
     * If the catalogue is not set, the future will fail with a ServiceException
     * If the resource is not part of the catalogue, but should be, the future will fail with a ServiceException
     *
     * @param id          the resource id
     * @param type        the resource type
     * @param catalogueId the catalogue id
     * @return a future that completes when the resource is deleted. If the resource is not found, the future will fail with a ServiceException
     */
    @Override
    public Future<Void> deleteResource(String id, String type, String catalogueId) {
        String graphName = baseUri + type + "/" + id;

        return validateCatalog(type, catalogueId)
                .compose(r -> {
                    if (r) {
                        String catalogueGraphName = DCATAPUriSchema.createFor(catalogueId).getGraphName();
                        String recordUriRef = DCATAPUriSchema.applyFor(id).getRecordUriRef();
                        return catalogueManager.removeResourceEntry(catalogueGraphName, graphName, recordUriRef)
                                .compose(v -> indexService.deleteResource(id, type))
                                .<Void>mapEmpty()
                                .recover(cause -> {
                                    if (cause.getMessage().contains("Not Found")) {
                                        throw new ServiceException(404, "Resource not found");
                                    } else {
                                        throw new ServiceException(500, cause.getMessage());
                                    }
                                });
                    } else {
                        return deleteSingleResource(id, type);
                    }
                });


    }


    /**
     * Create or update a resource. If the resource type requires a catalogue, this method will check if the catalogue is set.
     * If the catalogue is not set, the future will fail with a ServiceException
     *
     * @param id          the id of the resource
     * @param type        the type of the resource
     * @param catalogueId the id of the catalogue, if the resource type requires a catalogue, can be {@code null} or empty string otherwise
     * @param content     the model content
     * @param contentType the content type of the resource
     * @return a future that completes when the resource is created or updated. If the resource is not found, the future will fail with a ServiceException
     */
    @Override
    public Future<String> putResource(String id, String type, String catalogueId, String content, String contentType) {
        try {


            if (profile.extraResourceIsSet(type)) {
                Model model = Piveau.toModel(content.getBytes(), contentType);
                ExtraResource resource = profile.getExtraResource(type);

                //early return in case catalogue is needed but not provided
                if (resource.isInCatalog() && (catalogueId == null || catalogueId.isBlank())) {

                    return Future.failedFuture(new ServiceException(400, "Catalogue is required"));
                }
                // if we do not need catalogueId anymore, we can set it to null to make things easier to handle later on
                if (!resource.isInCatalog()) {
                    catalogueId = null;
                }

                Property typeProperty = ModelFactory.createDefaultModel().createProperty(resource.getTypeURI());

                String graphName = baseUri + type + "/" + id;

                AtomicReference<String> defLang = new AtomicReference<>("en");


                if (catalogueId != null && !catalogueId.isBlank()) {
                    String finalCatalogueId = catalogueId;

                    return catalogueLang(catalogueId).compose(lang -> {

                                defLang.set(lang);

                                String catalogueGraphName = DCATAPUriSchema.parseUriRef(finalCatalogueId).getGraphName();
                                String catalogueUriRef = DCATAPUriSchema.parseUriRef(finalCatalogueId).getUriRef();
                                String recordUriRef = DCATAPUriSchema.parseUriRef(id).getRecordUriRef();

                                return catalogueManager.addResourceEntry(
                                                catalogueGraphName,
                                                catalogueUriRef,
                                                graphName,
                                                recordUriRef)
                                        .onFailure(cause -> {
                                            serviceContext.log().error("Add catalogue entry for {}", id, cause);
                                            if (cause instanceof TripleStoreException te) {
                                                if (!te.getQuery().isBlank()) {
                                                    serviceContext.log().error("Query: {}", te.getQuery());
                                                }
                                            }
                                        });
                            })
                            .compose(unit -> store(id, type, model, typeProperty, graphName, resource, defLang, finalCatalogueId))
                            .onSuccess(ar -> serviceContext.log().info("Resource {} updated", id));
                } else {
                    return store(id,type,model,typeProperty,graphName,resource,defLang,null);

                }

            } else {
                return Future.failedFuture(new ServiceException(404, "Resource type unknown"));
            }


        } catch (Exception e) {
            log.error("Error creating custom resource:", e);
            return Future.failedFuture(new ServiceException(500, "Internal Server Error"));
        }
    }

    //TODO: this should use the resource entity
    private Future<String> store(String id, String type, Model model, Property typeProperty, String graphName, ExtraResource resource, AtomicReference<String> defLang, String finalCatalogueId) {
        ResIterator resIterator = model.listSubjectsWithProperty(RDF.type, typeProperty);
        if (resIterator.hasNext()) {
            resIterator.forEachRemaining(rs -> Piveau.rename(rs, graphName));
            return tripleStore.setGraph(graphName, model, true)
                    .onSuccess(setGraphResult -> indexResource(id, type, model, graphName, resource, defLang.get(), finalCatalogueId));

        } else {
            return Future.failedFuture(new ServiceException(400, "Resource type not found in payload"));
        }
    }

    /**
     * Index a resource and send to index service. This method is called after the resource is created or updated.
     *
     * @param id        the id of the resource
     * @param type      the type of the resource
     * @param model     the model of the resource
     * @param graphName the graph name of the resource
     * @param resource  the extra resource
     * @param lang      the language of the resource
     * @param catalogId the id of the catalogue, if the resource type requires a catalogue, can be {@code null} or empty string otherwise
     */
    private void indexResource(String id, String type, Model model, String graphName, ExtraResource resource, String lang, String catalogId) {

        Indexer.indexingResource(
                model.getResource(graphName),                       // dataset resource
                Piveau.findRecordAsResource(model),                                       // catalog record resource
                resource.getIndexingInstructions(),                 // instructions build from shacl
                lang,                                               // default language
                "other",                                            // schema
                false,                                              // generate modified and issued (now)
                catalogId,                                          // catalog id
                false                                               // set country from spatial
        ).onSuccess(indexingResult -> {
            log.info("*** Indexing success");
            log.info("Indexing instructions: {}", resource.getIndexingInstructions());
            log.info("Indexing Result: {}", indexingResult);
            indexService.putResource(id, type, indexingResult)
                    .onFailure(cause -> log.error("Failed to put resource: {}", id, cause));
        }).onFailure(ar -> log.error("Indexing Resource failed", ar));
    }

    /**
     * Get the language of a catalogue, if it exists.
     *
     * @param catalogueId the id of the catalogue
     * @return a future that completes with the language of the catalogue.
     * If the catalogue is not found, the future will fail with a ServiceException.
     * If the catalogue has no language, fallbacks will be used
     */
    private Future<String> catalogueLang(String catalogueId) {
        Promise<String> promise = Promise.promise();
        if (catalogueId == null || catalogueId.isBlank()) {
            return Future.failedFuture(new ServiceException(400,"Catalogue id is empty"));
        }

        if (cache.containsKey(catalogueId)) {
            return Future.succeededFuture(cache.get(catalogueId));
        }

        DCATAPUriRef catalogueUriRef = DCATAPUriSchema.createForCatalogue(catalogueId);
        String query = sparqlQueryPool.format("catalogueQuery", catalogueUriRef.getGraphName(), catalogueUriRef.getUriRef());
        if (query == null) {
            return Future.failedFuture(new ServiceException(500, "Missing query from pool"));
        }

        tripleStore.select(query)
                .onSuccess(result -> {
                    if (result.hasNext()) {
                        QuerySolution qs = result.next();
                        String langCode = null;

                        if (qs.contains("lang")) {
                            Concept concept = Languages.INSTANCE.getConcept(qs.getResource("lang"));
                            if (concept != null) {
                                langCode = Languages.INSTANCE.iso6391Code(concept);
                                if (langCode == null) {
                                    langCode = Languages.INSTANCE.tedCode(concept);
                                }

                            }
                        }
                        if (langCode == null) {
                            langCode = "en";
                        }
                        promise.complete(langCode.toLowerCase());
                    } else {
                        promise.fail(new ServiceException(404, "Catalogue not found"));
                    }
                })
                .onFailure(promise::fail);

        return promise.future();

    }


    //TODO: merge with same dataset function?
    /**
     * Get the get records and hashes for a dataset
     * @param catalogURI the uri of the catalogue
     * @param id         the id of the dataset
     * @return Future with list Json Objects containing `recordUri` and, if existing, `hash`
     */
    private Future<List<JsonObject>> getRecordHash(String catalogURI, String id) {
        String query = sparqlQueryPool.format(
                "recordQuery",
                catalogURI,
                catalogURI, //TODO: use resource entity here?
                id
        );

        if (query == null) {
            return Future.failedFuture(new ServiceException(500, "Missing query from pool"));
        }


        return tripleStore.select(query)
                .map(resultSet -> {
                    List<JsonObject> records = new ArrayList<>();
                    while (resultSet.hasNext()) {
                        QuerySolution solution = resultSet.next();
                        JsonObject info = new JsonObject().put("recordUri", solution.getResource("record").getURI());
                        if (solution.contains("hash")) {
                            info.put("hash", solution.getLiteral("hash").getLexicalForm());
                        }
                        records.add(info);
                    }
                    return records;
                });
    }


    /**
     * Validate if the resource type requires a catalogue and if the catalogue is set.
     * If the catalogue is not set, the future will fail with a ServiceException
     *
     * @param type        the type of the resource
     * @param catalogueId the id of the catalogue, if the resource type requires a catalogue, can be {@code null} or empty string otherwise
     * @return a future that completes with a boolean indicating if the catalogue is required and set.
     * If the catalogue is not required, the future will complete with false.
     * If the catalogue is required but not set, the future will fail with a ServiceException
     */
    private Future<Boolean> validateCatalog(String type, String catalogueId) {

        if (profile.extraResourceIsSet(type)) {
            //check if needs catalogue
            // if yes: check if catalogue is set

            ExtraResource resource = profile.getExtraResource(type);

            if (resource.isInCatalog()) {
                if (catalogueId == null || catalogueId.isBlank()) {
                    return Future.failedFuture(new ServiceException(400, "Catalogue is required"));
                }

                return resourceManager.catalog(baseUri + type + "/" + catalogueId)
                        .map(catalogResource -> {
                            if (catalogResource == null) {
                                throw new ServiceException(404, "Catalogue not found");

                            } else {
                                return true;
                            }
                        });

            } else {
                return Future.succeededFuture(false);
            }
        }
        return Future.failedFuture(new ServiceException(500, "Resource Type misconfigured"));
    }

    //TODO: move to resource entity
    /**
     * Calculate the hash of the dataset
     * @return the hash of the dataset,  or an empty string if an error occurred
     */
    public String hash(Model model) {
        try {
            return JenaUtils.canonicalHash(model);
        } catch (Exception e) {
            log.error("Calculating hash failure", e);
            return "";
        }
    }
}
