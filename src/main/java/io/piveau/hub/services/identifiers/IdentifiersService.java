package io.piveau.hub.services.identifiers;

import io.piveau.dcatap.TripleStore;
import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.WebClient;

@ProxyGen
public interface IdentifiersService {
    String SERVICE_ADDRESS = "io.piveau.hub.identifiers.queue";

    static Future<IdentifiersService> create(Vertx vertx, WebClient client, JsonObject config, TripleStore tripleStore) {
        return Future.future(promise -> new IdentifiersServiceImpl(vertx, client, config, tripleStore, promise));
    }

    static IdentifiersService createProxy(Vertx vertx, String address) {
        return new IdentifiersServiceVertxEBProxy(vertx, address, new DeliveryOptions().setSendTimeout(120000));
    }

    Future<JsonObject> createIdentifier(String datasetId, String catalogueId, String type);

    Future<JsonObject> checkIdentifierRequirement(String datasetId, String catalogueId, String type);

}
