package io.piveau.hub.services.index;

import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.client.WebClient;

@ProxyGen
public interface IndexService {
    String SERVICE_ADDRESS = "io.piveau.hub.index.queue";
    Long DEFAULT_TIMEOUT = 300000L;

    static Future<IndexService> create(WebClient client, JsonObject config) {
        return Future.future(promise -> new IndexServiceImpl(client, config, promise));
    }

    static IndexService createProxy(Vertx vertx, String address, Long timeout) {
        return new IndexServiceVertxEBProxy(vertx, address, new DeliveryOptions().setSendTimeout(timeout));
    }

    Future<JsonObject> addDataset(JsonObject dataset);

    Future<JsonObject> deleteDataset(String id);

    Future<JsonObject> getDataset(String id);

    Future<JsonObject> deleteCatalog(String id);

    Future<JsonObject> addCatalog(JsonObject catalog);

    Future<JsonArray> listDatasets(String catalogue, String alias);

    Future<JsonObject> addDatasetPut(JsonObject dataset);

    Future<JsonObject> modifyDataset(String datasetId, JsonObject dataset);

    Future<JsonArray> putVocabulary(String vocabularyId, JsonObject vocabulary);

    Future<JsonObject> deleteVocabulary(String vocabularyId);

    Future<JsonArray> putDatasetBulk(JsonArray datasets);

    Future<Void> putResource(String id, String type, JsonObject payload);

    Future<Void> deleteResource(String id, String type);

}
