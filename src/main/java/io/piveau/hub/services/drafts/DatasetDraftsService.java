package io.piveau.hub.services.drafts;

import io.piveau.dcatap.TripleStore;
import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.util.List;

@ProxyGen
public interface DatasetDraftsService {
    String SERVICE_ADDRESS = "io.piveau.hub.drafts.queue";

    static Future<DatasetDraftsService> create(TripleStore shadowTripleStore, JsonObject config, Vertx vertx) {
        return Future.future(promise -> new DatasetDraftsServiceImpl(shadowTripleStore, config, vertx, promise));
    }

    static DatasetDraftsService createProxy(Vertx vertx, String address) {
        return new DatasetDraftsServiceVertxEBProxy(vertx, address, new DeliveryOptions().setSendTimeout(120000));
    }

    Future<JsonArray> listDatasetDrafts(List<String> catalogueId, String provider);

    Future<String> createDatasetDraft(String catalogueId, String payload, String contentType, String provider);

    Future<String> readDatasetDraft(String datasetId, String catalogueId, String acceptType);

    Future<String> createOrUpdateDatasetDraft(String datasetId, String catalogueId, String payload,
                                                    String contentType, String provider);

    Future<Void> deleteDatasetDraft(String datasetId, String catalogueId);

}
