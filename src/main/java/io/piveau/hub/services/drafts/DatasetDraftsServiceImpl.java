package io.piveau.hub.services.drafts;

import io.piveau.dcatap.DCATAPUriRef;
import io.piveau.dcatap.DCATAPUriSchema;
import io.piveau.dcatap.Prefixes;
import io.piveau.dcatap.TripleStore;
import io.piveau.hub.dataobjects.DatasetHelper;
import io.piveau.rdf.Piveau;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.serviceproxy.ServiceException;
import org.apache.commons.lang3.StringUtils;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.sparql.vocabulary.FOAF;
import org.apache.jena.vocabulary.DCAT;
import org.apache.jena.vocabulary.DCTerms;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicReference;

public class DatasetDraftsServiceImpl implements DatasetDraftsService {

    private final TripleStore shadowTripleStore;


    private final Logger log = LoggerFactory.getLogger(getClass());

    DatasetDraftsServiceImpl(TripleStore shadowTripleStore, JsonObject config, Vertx vertx,
                             Handler<AsyncResult<DatasetDraftsService>> readyHandler) {
        this.shadowTripleStore = shadowTripleStore;


        readyHandler.handle(Future.succeededFuture(this));
    }

    @Override
    public Future<JsonArray> listDatasetDrafts(List<String> catalogueIds, String provider) {
        String filter = "";
        String creator = "";
        if (provider != null && !provider.isEmpty()) {
            filter = "FILTER(?creator = \"" + provider + "\")";
            creator = "; <" + DCTerms.creator + "> ?creator";
        } else if (catalogueIds != null && !catalogueIds.isEmpty()) {
            List<String> catalogueUriRefs = new ArrayList<>();
            for (String catalogueId : catalogueIds) {
                DCATAPUriRef catalogueUriRef = DCATAPUriSchema.createForCatalogue(catalogueId);
                catalogueUriRefs.add("<" + catalogueUriRef.getCatalogueUriRef() + ">");
            }
            filter = "FILTER(?catalog IN (" + String.join(",", catalogueUriRefs) + "))";
        }

        String query = "SELECT ?id ?catalog ?title ?description \nWHERE { \nGRAPH ?dataset { \n?record "
                + "<" + FOAF.primaryTopic + "> ?dataset;\n"
                + "<" + DCTerms.identifier + "> ?id;\n"
                + "<" + DCAT.catalog + "> ?catalog\n"
                + "OPTIONAL { ?dataset <" + DCTerms.title + "> ?title }\n"
                + "OPTIONAL { ?dataset <" + DCTerms.description + "> ?description }\n"
                + creator + "}\n"
                + filter + "}";

        return shadowTripleStore.select(query)
                .map(result -> {
                    JsonArray jsonResult = new JsonArray();
                    while (result.hasNext()) {
                        QuerySolution qs = result.next();

                        String id = qs.getLiteral("id").getLexicalForm();
                        String catalog = StringUtils.substringAfterLast(qs.getResource("catalog").getURI(), "/");

                        JsonObject dataset = null;

                        for (Object o : jsonResult) {
                            JsonObject jsonObject = (JsonObject) o;
                            if (jsonObject.getString("id").equals(id)
                                    && jsonObject.getString("catalog").equals(catalog)) dataset = jsonObject;
                        }

                        if (dataset == null) {
                            dataset = new JsonObject();
                            dataset.put("id", id);
                            dataset.put("catalog", catalog);
                            jsonResult.add(dataset);
                        }

                        if (qs.getLiteral("title") != null) {
                            if (dataset.getJsonObject("title") == null) {
                                dataset.put("title", new JsonObject());
                            }

                            dataset.getJsonObject("title").put(
                                    qs.getLiteral("title").getLanguage(),
                                    qs.getLiteral("title").getLexicalForm()
                            );
                        }

                        if (qs.getLiteral("description") != null) {
                            if (dataset.getJsonObject("description") == null) {
                                dataset.put("description", new JsonObject());
                            }

                            dataset.getJsonObject("description").put(
                                    qs.getLiteral("description").getLanguage(),
                                    qs.getLiteral("description").getLexicalForm()
                            );
                        }
                    }
                    return jsonResult;
                })
                .recover(cause -> {
                    throw new ServiceException(500, cause.getMessage());
                });
    }

    @Override
    public Future<String> createDatasetDraft(String catalogueId, String payload, String contentType, String provider) {
        String datasetId = UUID.randomUUID().toString();
        return createOrUpdateDatasetDraft(datasetId, catalogueId, payload, contentType, provider);
    }

    @Override
    public Future<String> readDatasetDraft(String datasetId, String catalogueId, String acceptType) {
        DCATAPUriRef catalogueUriRef = DCATAPUriSchema.createForCatalogue(catalogueId);

        String query = "SELECT ?dataset WHERE { GRAPH ?dataset { ?record "
                + "<" + FOAF.primaryTopic + "> ?dataset;"
                + "<" + DCTerms.identifier + "> \"" + datasetId + "\" ;"
                + "<" + DCAT.catalog + "> <" + catalogueUriRef.getCatalogueUriRef() + "> } }";

        return shadowTripleStore.select(query)
                .compose(result -> {
                    if (result.hasNext()) {
                        String uri = result.next().getResource("dataset").getURI();
                        return shadowTripleStore.getGraph(uri);
                    } else {
                        return Future.failedFuture(new ServiceException(404, "Dataset draft not found"));
                    }
                })
                .map(model -> {
                    if (model.isEmpty()) {
                        throw new ServiceException(404, "Dataset draft not found");
                    } else {
                        Prefixes.setNsPrefixesFiltered(model);
                        return Piveau.presentAs(model, acceptType);
                    }
                })
                .recover(cause -> {
                    if (cause instanceof ServiceException se) {
                        throw se;
                    } else {
                        throw new ServiceException(500, cause.getMessage());
                    }
                });
    }

    @Override
    public Future<String> createOrUpdateDatasetDraft(String datasetId, String catalogueId, String payload,
                                                     String contentType, String provider) {
        log.debug("Create or update draft dataset");
        DatasetHelper datasetHelper = DatasetHelper.create(datasetId, payload, contentType, catalogueId);
        return findPiveauId(datasetHelper.piveauId(), catalogueId, 0)
                .compose(id -> {
                    log.debug("Found piveau id: {}", id);
                    datasetHelper.init(id);

                    DCATAPUriRef catalogueUriRef = DCATAPUriSchema.createForCatalogue(catalogueId);
                    Model model = datasetHelper.model();
                    Resource catalogRecord = model.getResource(datasetHelper.recordUriRef());
                    Resource catalogResource = model.createResource(catalogueUriRef.getCatalogueUriRef());

                    model.removeAll(null,DCAT.catalog, null);
                    Statement catalogStatement = model.createStatement(catalogRecord, DCAT.catalog, catalogResource);
                    model.add(catalogStatement);

                    if (!provider.isEmpty()) {
                        Statement providerStatement = model.createStatement(catalogRecord, DCTerms.creator, provider);
                        model.add(providerStatement);
                    }

                    log.debug("Store draft dataset in shadow triple store");
                    return shadowTripleStore.setGraph(datasetHelper.graphName(), model, true)
                            .onSuccess(setGraphResult -> log.debug("Successfully stored."));
                })
                .recover(cause -> {
                    throw new ServiceException(500, cause.getMessage());
                });
    }

    @Override
    public Future<Void> deleteDatasetDraft(String datasetId, String catalogueId) {
        DCATAPUriRef catalogueUriRef = DCATAPUriSchema.createForCatalogue(catalogueId);

        String query = "SELECT ?dataset WHERE { GRAPH ?dataset { ?record "
                + "<" + FOAF.primaryTopic + "> ?dataset;"
                + "<" + DCTerms.identifier + "> \"" + datasetId + "\" ;"
                + "<" + DCAT.catalog + "> <" + catalogueUriRef.getCatalogueUriRef() + "> } }";

        return shadowTripleStore.select(query)
                .compose(result -> {
                    if (result.hasNext()) {
                        String uri = result.next().getResource("dataset").getURI();
                        return shadowTripleStore.deleteGraph(uri).<Void>mapEmpty();
                    } else {
                        return Future.failedFuture(new ServiceException(404, "Dataset draft not found"));
                    }
                })
                .recover(cause -> {
                    if (cause instanceof ServiceException se) {
                        throw se;
                    } else {
                        throw new ServiceException(500, cause.getMessage());
                    }
                });
    }

    private Future<String> findPiveauId(String datasetId, String catalogueId, int counter) {
        AtomicReference<String> checkId = new AtomicReference<>();
        if (counter > 0) {
            checkId.set(datasetId + "~~" + counter);
        } else {
            checkId.set(datasetId);
        }

        String datasetUriRef = DCATAPUriSchema.createForDataset(checkId.get()).getDatasetUriRef();
        String catalogueUriRef = DCATAPUriSchema.createForCatalogue(catalogueId).getCatalogueUriRef();

        String query1 = "ASK WHERE { GRAPH ?dataset { ?record "
                + "<" + FOAF.primaryTopic + "> <" + datasetUriRef + ">;"
                + "<" + DCTerms.identifier + "> \"" + datasetId + "\" ;"
                + "<" + DCAT.catalog + "> ?catalog } }";

        String query2 = "ASK WHERE { GRAPH ?dataset { ?record "
                + "<" + FOAF.primaryTopic + "> <" + datasetUriRef + ">;"
                + "<" + DCTerms.identifier + "> \"" + datasetId + "\" ;"
                + "<" + DCAT.catalog + "> <" + catalogueUriRef + "> } }";

        return shadowTripleStore.ask(query1)
                .compose(exist1 -> {
                    if (exist1 == Boolean.TRUE) {
                        return shadowTripleStore.ask(query2)
                                .compose(exist2 -> {
                                    if (exist2 == Boolean.TRUE) {
                                        return Future.succeededFuture(checkId.get());
                                    } else {
                                        return findPiveauId(datasetId, catalogueId, counter + 1);
                                    }
                                });
                    } else {
                        return Future.succeededFuture(checkId.get());
                    }
                })
                .recover(cause -> {
                    throw new ServiceException(500, cause.getMessage());
                });
    }

}
