package io.piveau.hub.services.datasets;

import io.piveau.HubRepo;
import io.piveau.dcatap.*;
import io.piveau.dga.AddOns;
import io.piveau.hub.Constants;
import io.piveau.hub.Defaults;
import io.piveau.hub.dataobjects.DatasetHelper;
import io.piveau.hub.indexing.Indexing;
import io.piveau.hub.services.index.IndexService;
import io.piveau.hub.services.translation.TranslationService;
import io.piveau.hub.util.DataUploadConnector;
import io.piveau.json.ConfigHelper;
import io.piveau.log.PiveauLogger;
import io.piveau.pipe.PipeLauncher;
import io.piveau.rdf.HydraCollection;
import io.piveau.rdf.Piveau;
import io.piveau.rdf.RDFMimeTypes;
import io.piveau.sparql.SparqlQueryPool;
import io.piveau.utils.PiveauContext;
import io.piveau.vocabularies.Concept;
import io.piveau.vocabularies.Languages;
import io.piveau.vocabularies.vocabulary.LOCN;
import io.vertx.core.*;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.serviceproxy.ServiceException;
import org.apache.jena.graph.NodeFactory;
import org.apache.jena.query.Dataset;
import org.apache.jena.query.DatasetFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.rdf.model.*;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RiotException;
import org.apache.jena.shared.DoesNotExistException;
import org.apache.jena.vocabulary.DCAT;
import org.apache.jena.vocabulary.RDF;
import org.ehcache.Cache;
import org.ehcache.CacheManager;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.CacheManagerBuilder;
import org.ehcache.config.builders.ExpiryPolicyBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.config.units.EntryUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.file.Path;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

public class DatasetsServiceImpl implements DatasetsService {

    private static final String DGA_SUPER_CATALOGUE = "http://data.europa.eu/88u/catalogue/erpd";

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final PiveauContext piveauContext = new PiveauContext("hub", "repo");

    private final PiveauContext dgaContext = new PiveauContext("dga", "dga-validation");

    private final TripleStore tripleStore;
    private final TripleStore shadowTripleStore;

    private final CatalogueManager catalogueManager;
    private final DatasetManager datasetManager;

    private final DataUploadConnector dataUploadConnector;

    private IndexService indexService;
    private TranslationService translationService;

    private final JsonObject validationConfig;

    private final PipeLauncher launcher;

    private final boolean prependXmlDeclaration;

    private final Cache<String, JsonObject> cache;
    private final Cache<String, Integer> hydraCache;
    private static final String TOTAL_DATASET_COUNT = "TOTAL_DATASET_COUNT";


    private final Set<String> clearGeoDataCatalogues = new HashSet<>();

    private Boolean forceUpdates = Defaults.FORCE_UPDATES;

    private Boolean archivingEnabled = Defaults.EXPERIMENTS;

    private SparqlQueryPool sparqlQueryPool;

    DatasetsServiceImpl(TripleStore tripleStore, TripleStore shadowTripleStore, DataUploadConnector dataUploadConnector, JsonObject config, PipeLauncher launcher, Vertx vertx, Handler<AsyncResult<DatasetsService>> readyHandler) {

        CacheManager cacheManager = CacheManagerBuilder.newCacheManagerBuilder()
                .withCache("datasetsService", CacheConfigurationBuilder.newCacheConfigurationBuilder(String.class, JsonObject.class,
                                ResourcePoolsBuilder.newResourcePoolsBuilder().heap(100, EntryUnit.ENTRIES))
                        .withExpiry(ExpiryPolicyBuilder.timeToIdleExpiration(Duration.ofHours(12))))
                .withCache("totalCount", CacheConfigurationBuilder.newCacheConfigurationBuilder(String.class, Integer.class,
                                ResourcePoolsBuilder.newResourcePoolsBuilder().heap(100, EntryUnit.ENTRIES))
                        .withExpiry(ExpiryPolicyBuilder.timeToIdleExpiration(Duration.ofHours(12))))
                .build(true);

        cache = cacheManager.getCache("datasetsService", String.class, JsonObject.class);
        hydraCache = cacheManager.getCache("totalCount", String.class, Integer.class);


        this.launcher = launcher;

        this.tripleStore = tripleStore;
        this.shadowTripleStore = shadowTripleStore;

        ConfigHelper configHelper = ConfigHelper.forConfig(config);
        JsonArray list =
                configHelper.forceJsonObject(Constants.ENV_PIVEAU_TRIPLESTORE_CONFIG)
                        .getJsonArray("clearGeoDataCatalogues", new JsonArray());
        List<String> entries = list.stream().map(Object::toString).toList();
        clearGeoDataCatalogues.addAll(entries);

        forceUpdates = config.getBoolean(Constants.ENV_PIVEAU_HUB_FORCE_UPDATES, forceUpdates);

        archivingEnabled = config.getBoolean(Constants.ENV_PIVEAU_ARCHIVING_ENABLED, archivingEnabled);


        catalogueManager = tripleStore.getCatalogueManager();
        datasetManager = tripleStore.getDatasetManager();

        this.dataUploadConnector = dataUploadConnector;

        JsonObject indexConfig = configHelper.forceJsonObject(Constants.ENV_PIVEAU_HUB_SEARCH_SERVICE);
        if (indexConfig.getBoolean("enabled", Defaults.SEARCH_SERVICE_ENABLED)) {
            indexService = IndexService.createProxy(vertx, IndexService.SERVICE_ADDRESS, IndexService.DEFAULT_TIMEOUT);
        }
        JsonObject translationConfig = configHelper.forceJsonObject(Constants.ENV_PIVEAU_TRANSLATION_SERVICE);
        if (translationConfig.getBoolean("enable", Defaults.TRANSLATION_SERVICE_ENABLED)) {
            translationService = TranslationService.createProxy(vertx, TranslationService.SERVICE_ADDRESS);
        }

        validationConfig = configHelper.forceJsonObject(Constants.ENV_PIVEAU_HUB_VALIDATOR);

        prependXmlDeclaration = config.getBoolean(Constants.ENV_PIVEAU_HUB_XML_DECLARATION, Defaults.XML_DECLARATION);

        sparqlQueryPool = SparqlQueryPool.Companion.create(vertx, Path.of("queries"));

        readyHandler.handle(Future.succeededFuture(this));
    }

    @Override
    public Future<String> listCatalogueDatasets(String acceptType, String valueType, String catalogueId, Integer limit, Integer offset) {
        try {
            return catalogueManager.exists(catalogueId)
                    .compose(exists -> {
                        if (!exists) {
                            return Future.failedFuture(new ServiceException(404, "Catalogue not found"));
                        } else {
                            switch (valueType) {
                                case "originalIds" -> {
                                    return catalogueManager.allDatasetIdentifiers(catalogueId)
                                            .map(list -> new JsonArray(list).encodePrettily());
                                }
                                case "identifiers" -> {
                                    return catalogueManager.datasets(catalogueId, offset, limit)
                                            .map(list -> new JsonArray(list.stream().map(DCATAPUriRef::getId).toList()).encodePrettily());
                                }
                                case "uriRefs" -> {
                                    return catalogueManager.datasets(catalogueId, offset, limit)
                                            .map(list -> new JsonArray(list.stream().map(DCATAPUriRef::getUri).toList()).encodePrettily());
                                }
                                case "metadata" -> {
                                    return catalogueManager.listDatasets(catalogueId, offset, limit)
                                            .map(list -> {
                                                list.forEach(this::changeVirtuType);
                                                return Piveau.presentAs(list, acceptType, DCAT.Dataset);
                                            });
                                }
                                default -> {
                                    return Future.failedFuture(new ServiceException(400, "Unknown value type"));
                                }
                            }
                        }
                    });
        } catch (Exception e) {
            return Future.failedFuture(new ServiceException(500, "Internal Server Error"));
        }
    }

    @Override
    public Future<String> listDatasets(String acceptType, String valueType, Integer limit, Integer offset, Boolean hydra, String base) {
        try {
            switch (valueType) {
                case "originalIds" -> {
                    return Future.failedFuture(new ServiceException(400, "Value type originalIds only allowed when catalogueId is set"));
                }
                case "identifiers" -> {
                    return datasetManager.list(offset, limit)
                            .map(list -> new JsonArray(list.stream().map(DCATAPUriRef::getId).toList()).encodePrettily());
                }
                case "uriRefs" -> {
                    return datasetManager.list(offset, limit)
                            .map(list -> new JsonArray(list.stream().map(DCATAPUriRef::getUri).toList()).encodePrettily());
                }
                case "metadata" -> {
                    return datasetManager.listModels(offset, limit)
                            .compose(list -> {
                                list.forEach(this::changeVirtuType);
                                if (hydra) {
                                    if (hydraCache.containsKey(TOTAL_DATASET_COUNT)) {
                                        HydraCollection hydraCollection = HydraCollection.build(base, hydraCache.get(TOTAL_DATASET_COUNT), offset, limit, list);
                                        return Future.succeededFuture(Piveau.presentAs(list, hydraCollection, acceptType, DCAT.Dataset));
                                    } else {
                                        return datasetManager.count().compose(count -> {
                                            HydraCollection hydraCollection = HydraCollection.build(base, count, offset, limit, list);
                                            hydraCache.put(TOTAL_DATASET_COUNT, count);
                                            return Future.succeededFuture(Piveau.presentAs(list, hydraCollection, acceptType, DCAT.Dataset));
                                        });
                                    }

                                } else {
                                    return Future.succeededFuture(Piveau.presentAs(list, acceptType, DCAT.Dataset));
                                }
                            });
                }
                default -> {
                    return Future.failedFuture(new ServiceException(400, "Unknown value type"));
                }
            }
        } catch (Exception e) {
            return Future.failedFuture(new ServiceException(500, "Internal Server Error"));
        }
    }


    private Model changeVirtuType(Model model) {
        List<Statement> stmtList = new ArrayList<>();
        List<Statement> stmtListToRemove = new ArrayList<>();

        logger.trace("listResourcesWithProperty");
        model.listResourcesWithProperty(LOCN.geometry).forEach(res -> {
            logger.trace("RES: {}", res);
            res.listProperties(LOCN.geometry).forEach(stmt -> {
                logger.trace("STMT: {}", stmt);
                if (stmt.getObject().isLiteral()) {
                    Literal l = stmt.getLiteral();
                    logger.trace("DATATYPE: {}", l.getDatatype().getURI());
                    if (l.getDatatype().getURI().equals("http://www.openlinksw.com/schemas/virtrdf#Geometry")) {

                        stmtListToRemove.add(stmt);
                        String value = l.getString();
                        Literal modifiedLiteral = ResourceFactory.createTypedLiteral(value,
                                NodeFactory.getType("http://www.opengis.net/ont/geosparql#wktLiteral"));

                        Statement newStmt = ResourceFactory.createStatement(stmt.getSubject(), stmt.getPredicate(), modifiedLiteral);

                        stmtList.add(newStmt);
                    }

                }
            });
        });
        stmtListToRemove.forEach(model::remove);
        stmtList.forEach(model::add);
        return model;
    }

    @Override
    public Future<String> getDatasetOrigin(String originId, String catalogueId, String acceptType) {
        try {
            return catalogueManager.exists(catalogueId)
                    .compose(exists -> {
                        if (exists) {
                            return datasetManager.get(originId, catalogueId);
                        } else {
                            return Future.failedFuture(new ServiceException(404, "Catalogue not found"));
                        }
                    })
                    .map(model -> {
                        if (model.isEmpty()) {
                            throw new ServiceException(404, "Dataset not found");
                        } else {
                            changeVirtuType(model);
                            return Piveau.presentAs(model, acceptType);
                        }
                    })
                    .recover(cause -> {
                        throw mapCause(cause);
                    });
        } catch (Exception e) {
            return Future.failedFuture(new ServiceException(500, "Internal Server Error"));
        }
    }

    @Override
    public Future<String> getDataset(String id, String acceptType) {
        try {
            DCATAPUriRef uriRef = DCATAPUriSchema.createFor(id, DCATAPUriSchema.getDatasetContext());
            return datasetManager.getGraph(uriRef.getGraphName())
                    .map(model -> {
                        if (model.isEmpty()) {
                            throw new ServiceException(404, "Dataset not found");
                        } else {
                            changeVirtuType(model);
                            Prefixes.setNsPrefixesFiltered(model);
                            return Piveau.presentAs(model, acceptType);
                        }
                    });
        } catch (Exception e) {
            return Future.failedFuture(new ServiceException(500, "Internal Server Error"));
        }
    }

    @Override
    public Future<String> getRecord(String id, String acceptType) {
        try {
            DCATAPUriRef uriRef = DCATAPUriSchema.createForDataset(id);
            return datasetManager.getGraph(uriRef.getDatasetGraphName())
                    .map(model -> {
                        if (model.isEmpty()) {
                            throw new ServiceException(404, "Catalogue record not found");
                        } else {
                            if (Piveau.isQuadFormat(acceptType)) {
                                Dataset dataset = DatasetFactory.create(model);
                                return Piveau.presentAs(dataset, acceptType);
                            } else {
                                return Piveau.presentAs(model, acceptType);
                            }
                        }
                    });
        } catch (Exception e) {
            return Future.failedFuture(new ServiceException(500, "Internal Server Error"));
        }
    }

    @Override
    public Future<JsonObject> putDataset(String datasetId, String content, String contentType) {
        AtomicReference<DatasetHelper> datasetHelper = new AtomicReference<>();
        AtomicReference<JsonObject> catalogueInfo = new AtomicReference<>();
        AtomicReference<String> catalogueId = new AtomicReference<>();
        DCATAPUriRef datasetUriRef = DCATAPUriSchema.createForDataset(datasetId);
        hydraCache.clear();
        try {
            return datasetManager.existGraph(datasetUriRef.getGraphName())
                    .compose(exists -> {
                        if (!exists) {
                            return Future.failedFuture(new ServiceException(404, "Dataset not found"));
                        } else {
                            return datasetManager.catalogue(datasetUriRef.getDatasetUriRef());
                        }
                    })
                    .compose(catalog -> {
                        String catId = DCATAPUriSchema.parseUriRef(catalog.getURI()).getId();
                        catalogueId.set(catId);
                        return catalogueInfo(catId);
                    })
                    .compose(info -> {
                        catalogueInfo.set(info);
                        try {
                            return DatasetHelper.create(content, contentType);
                        } catch (Exception e) {
                            logger.error("Error parsing Dataset", e);
                            return Future.failedFuture(new ServiceException(400, "Could not decode body with Content-Type " + contentType + ": " + e.getMessage()));
                        }
                    })
                    .compose(helper -> {
                        datasetHelper.set(helper);
                        datasetHelper.get().catalogueId(catalogueId.get());
                        datasetHelper.get().sourceLang(catalogueInfo.get().getString("langCode"));
                        datasetHelper.get().sourceType(catalogueInfo.get().getString("type"));
                        datasetHelper.get().visibility(catalogueInfo.get().getString("visibility", ""));
                        return updateDataset(helper, datasetUriRef.getRecordUriRef());
                    })
                    .compose(this::store)
                    .map(result -> {
                        if (indexService != null) {
                            index(datasetHelper.get())
                                    .onFailure(cause -> {
                                        if (cause instanceof ServiceException serviceException) {
                                            logger.error("Indexing failure: {}", serviceException.getDebugInfo().encode(), cause);
                                        }
                                    });
                        }
                        if (validationConfig.getBoolean("enabled", Defaults.VALIDATOR_ENABLED)) {
                            systemPipes(datasetHelper.get());
                        }
                        //Log to archive
                        if (archivingEnabled) {
                            piveauContext.extend(datasetId).log().info("[Dataset] [0] [updated] " + Piveau.presentAsRdfXml(datasetHelper.get().model()));
                        }
                        return result;
                    });
        } catch (Exception e) {
            return Future.failedFuture(new ServiceException(500, "Internal Server Error"));
        }
    }

    @Override
    public Future<JsonObject> putDatasetOrigin(String originalId, String content, String contentType, String catalogueId, Boolean createAccessURLs) {
        PiveauLogger logPiveau = piveauContext.extend(originalId).log();
        AtomicReference<DatasetHelper> datasetHelper = new AtomicReference<>();
        if (hydraCache.containsKey(catalogueId)) hydraCache.remove(catalogueId);
        hydraCache.remove(TOTAL_DATASET_COUNT);
        Future<DatasetHelper> datasetHelperFuture = DatasetHelper.create(originalId, content, contentType, catalogueId);

        Future<JsonObject> catalogueInfo = catalogueInfo(catalogueId);

        return Future.all(datasetHelperFuture, catalogueInfo)
                .compose(f -> {
                    datasetHelper.set(datasetHelperFuture.result());
                    JsonObject catalogue = catalogueInfo.result();

                    datasetHelper.get().sourceLang(catalogue.getString("langCode"));
                    datasetHelper.get().sourceType(catalogue.getString("type"));
                    datasetHelper.get().visibility(catalogue.getString("visibility", ""));

                    // add dga validation here
                    if (catalogue.containsKey("superCatalogue")
                            && catalogue.getString("superCatalogue").equals(DGA_SUPER_CATALOGUE)) {
                        Resource datasetResource = datasetHelper.get().model()
                                .listResourcesWithProperty(RDF.type, DCAT.Dataset).toList().get(0);
                        JsonObject report = AddOns.validationReport(datasetResource);
                        if (!report.isEmpty()) {
                            // log violations here
                            dgaContext.extend(catalogueId + "/" + originalId).log().warn(report.encode());
                        }
                    }

                    return getRecordHash(datasetHelper.get());
                })
                .compose(records -> {
                    if (records.isEmpty()) {
                        return createDataset(datasetHelper.get(), createAccessURLs);
                    } else {
                        if (records.size() > 1) {
                            logPiveau.warn("Found duplicate entries: {}", records.size());
                            // deleting duplicates... keep one. but how?
                            Iterator<JsonObject> it = records.iterator();
                            while (it.hasNext()) {
                                JsonObject record = it.next();
                                DCATAPUriRef uriRef = DCATAPUriSchema.parseUriRef(record.getString("recordUri"));

                                if (!uriRef.getId().equals(Piveau.asNormalized(datasetHelper.get().originId()))) {
                                    delete(uriRef).onComplete(ar -> logPiveau.warn("Duplicate {} removed", uriRef.getDatasetUriRef()));
                                    it.remove();
                                }
                            }
                        }
                        if (records.isEmpty()) {
                            return Future.failedFuture(new ServiceException(500, "Due to unclear duplicate detection removed completely"));
                        } else {
                            JsonObject recordInfo = records.get(0);
                            String recordUri = recordInfo.getString("recordUri");
                            if (!DCATAPUriSchema.isRecordUriRef(recordUri)) {
                                return Future.failedFuture(new ServiceException(500, "Foreign catalogue record with dct:identifier detected"));
                            } else {
                                String oldHash = recordInfo.getString("hash");
                                String currentHash = datasetHelper.get().hash();
                                if (currentHash.equals(oldHash) && !forceUpdates) {
                                    return Future.failedFuture(new ServiceException(304, "Dataset is up to date"));
                                } else {
                                    return updateDataset(datasetHelper.get(), recordUri);
                                }
                            }
                        }
                    }
                })
                .compose(finalHelper -> {
                    datasetHelper.set(finalHelper);
                    return store(datasetHelper.get());
                })
                .recover(cause -> {
                    throw mapCause(cause);
                })
                .onSuccess(status -> {
                    if (!datasetHelper.get().visibility().equals("hidden") && indexService != null) {
                        index(datasetHelper.get())
                                .onFailure(cause -> {
                                    if (cause instanceof ServiceException serviceException) {
                                        logPiveau.error("Indexing failure: {}", serviceException.getDebugInfo().encode(), cause);
                                    }
                                });
                    }
                    if (validationConfig.getBoolean("enabled", Defaults.VALIDATOR_ENABLED)) {
                        systemPipes(datasetHelper.get());
                    }
                    if (archivingEnabled) {
                        piveauContext.extend(datasetHelper.get().piveauId()).log().info("[Dataset] [{}] [{}] {}", catalogueId, status.getString("status"), Piveau.presentAsRdfXml(datasetHelper.get().model()));
                    }
                });
    }

    @Override
    public Future<JsonObject> postDataset(String content, String contentType, String catalogueId, Boolean createAccessURLs) {
        try {
            return putDatasetOrigin(UUID.randomUUID().toString(), content, contentType, catalogueId, createAccessURLs);
        } catch (Exception e) {
            return Future.failedFuture(new ServiceException(500, "Internal Server Error"));
        }
    }

    @Override
    public Future<Void> deleteDataset(String id) {
        try {
            return delete(DCATAPUriSchema.createFor(id));
        } catch (Exception e) {
            return Future.failedFuture(new ServiceException(500, "Internal Server Error"));
        }
    }

    @Override
    public Future<Void> deleteDatasetOrigin(String originId, String catalogueId) {
        try {
            return catalogueManager.exists(catalogueId)
                    .compose(exists -> {
                        if (exists) {
                            return datasetManager.identify(originId, catalogueId);
                        } else {
                            return Future.failedFuture(new ServiceException(404, "Catalogue not found"));
                        }
                    })
                    .compose(pair -> delete(DCATAPUriSchema.parseUriRef(pair.getFirst().getURI())))
                    .recover(cause -> {
                        throw mapCause(cause);
                    });
        } catch (Exception e) {
            return Future.failedFuture(new ServiceException(500, "Internal Server Error"));
        }
    }

    private Future<Void> delete(DCATAPUriRef schema) {
        hydraCache.clear();
        return datasetManager.deleteGraph(schema.getDatasetGraphName())
                .compose(v -> datasetManager.catalogue(schema.getDatasetUriRef()))
                .compose(catalogue -> catalogueManager.removeDatasetEntry(catalogue.getURI(), schema.getDatasetUriRef()))
                .map(v -> {
                    if (indexService != null) {
                        indexService.deleteDataset(schema.getId(), ar -> {
                            if (ar.failed()) {
                                logger.warn("Delete index", ar.cause());
                            }
                        });
                    }
                    if (validationConfig.getBoolean("enabled", Defaults.VALIDATOR_ENABLED)) {
                        tripleStore.deleteGraph(schema.getMetricsGraphName())
                                .onFailure(cause -> logger.warn("Delete metrics graph", cause));

                        shadowTripleStore.deleteGraph(schema.getHistoricMetricsGraphName())
                                .onFailure(cause -> logger.warn("Delete metrics history graph", cause));
                    }
                    return null;
                });
    }

    @Override
    public Future<JsonObject> indexDataset(String datasetId, String catalogueId) {
        if (indexService == null) {
            return Future.failedFuture("Indexing service disabled");
        }
        AtomicReference<JsonObject> catalogueInfo = new AtomicReference<>();
        String contentType = RDFMimeTypes.NTRIPLES;

        DCATAPUriRef datasetUriRef = DCATAPUriSchema.createForDataset(datasetId);

        return datasetManager.existGraph(datasetUriRef.getGraphName())
                .compose(exists -> {
                    if(!exists) {
                        return Future.failedFuture(new ServiceException(404, "Dataset not found"));
                    } else {
                        return catalogueInfo(catalogueId);
                    }
                })
                .compose(info -> {
                    catalogueInfo.set(info);
                    return datasetManager.getGraph(datasetUriRef.getGraphName());
                })
                .compose(dataset -> {
                    try {
                        return DatasetHelper.create(dataset);
                    } catch (Exception e) {
                        logger.error("Error parsing Dataset", e);
                        return Future.failedFuture(new ServiceException(400, "Could not decode body with Content-Type " + contentType + ": " + e.getMessage()));
                    }
                })
                .compose(helper -> {
                    JsonObject indexObject = Indexing.indexingDataset(
                            helper.resource(),
                            helper.recordResource(),
                            catalogueId,
                            catalogueInfo.get().getString("langCode"));
                    return Future.future(promise -> indexService.addDatasetPut(indexObject, ir -> {
                        if (ir.failed()) {
                            promise.fail(ir.cause());
                        } else {
                           promise.complete(ir.result());
                        }
                    }));
                });
    }

    @Override
    public Future<JsonObject> getDataUploadInformation(String datasetId, String catalogueId, String resultDataset) {
        try {
            return DatasetHelper.create(resultDataset, Lang.NTRIPLES.getHeaderString())
                    .map(helper -> {
                        JsonArray uploadResponse = dataUploadConnector.getResponse(helper);
                        JsonObject result = new JsonObject();
                        result.put("status", "success");
                        result.put("distributions", uploadResponse);
                        return result;
                    });
        } catch (Exception e) {
            return Future.failedFuture(new ServiceException(500, "Internal Server Error"));
        }
    }

    private Future<List<JsonObject>> getRecordHash(DatasetHelper helper) {
        String query = sparqlQueryPool.format(
                "recordQuery",
                helper.catalogueGraphName(),
                helper.catalogueUriRef(),
                helper.originId()
        );

        return tripleStore.select(query)
                .map(resultSet -> {
                    List<JsonObject> records = new ArrayList<>();
                    while (resultSet.hasNext()) {
                        QuerySolution solution = resultSet.next();
                        JsonObject info = new JsonObject().put("recordUri", solution.getResource("record").getURI());
                        if (solution.contains("hash")) {
                            info.put("hash", solution.getLiteral("hash").getLexicalForm());
                        }
                        records.add(info);
                    }
                    return records;
                });
    }

    private Future<JsonObject> store(DatasetHelper helper) {
        boolean clearGeoData = clearGeoDataCatalogues.contains(helper.catalogueId()) || clearGeoDataCatalogues.contains("*");
        return datasetManager.setGraph(helper.graphName(), helper.model(), clearGeoData)
                .map(result -> {
                    switch (result) {
                        case "created" -> {
                            return new JsonObject()
                                    .put("status", "created")
                                    .put("id", helper.piveauId())
                                    .put("dataset", helper.stringify(Lang.NTRIPLES))
                                    .put(HttpHeaders.LOCATION.toString(), helper.uriRef());
                        }
                        case "updated" -> {
                            return new JsonObject().put("status", "updated");
                        }
                        default -> throw new ServiceException(500, result);
                    }
                });
    }

    private Future<DatasetHelper> index(DatasetHelper helper) {
        return Future.future(promise -> {
            try {
                JsonObject indexMessage = Indexing.indexingDataset(helper.resource(), helper.recordResource(), helper.catalogueId(), helper.sourceLang());
                indexService.addDatasetPut(indexMessage, ar -> {
                    if (ar.succeeded()) {
                        promise.complete(helper);
                    } else {
                        promise.fail(ar.cause());
                    }
                });
            } catch (Exception e) {
                promise.fail(new ServiceException(500, e.getMessage()));
            }
        });
    }

    private void systemPipes(DatasetHelper helper) {
        String metricsPipe = validationConfig.getString("metricsPipeName", Defaults.METRICS_PIPE);
        if (launcher.isPipeAvailable(metricsPipe)) {
            JsonObject dataInfo = new JsonObject()
                    .put("originalId", helper.originId())
                    .put("catalogue", helper.catalogueId())
                    .put("identifier", helper.piveauId())
                    .put("source", helper.sourceType());

            Model model = HubRepo.extractDatasetModel(helper.model());
            Dataset dataset = DatasetFactory.create(model);
            launcher.runPipeWithData(metricsPipe, Piveau.presentAs(dataset, Lang.TRIG), RDFMimeTypes.TRIG, dataInfo, new JsonObject(), null);
        }
    }

    private Future<DatasetHelper> createDataset(DatasetHelper datasetHelper, boolean createAccessURLs) {
        return findPiveauId(datasetHelper.piveauId(), 0)
                .compose(id -> {
                    datasetHelper.init(id);

                    if (createAccessURLs) {
                        datasetHelper.setAccessURLs(dataUploadConnector);
                    }

                    catalogueManager.addDatasetEntry(
                                    datasetHelper.catalogueGraphName(),
                                    datasetHelper.catalogueUriRef(),
                                    datasetHelper.uriRef(),
                                    datasetHelper.recordUriRef())
                            .onFailure(cause -> {
                                logger.error("Add catalogue entry for {}", datasetHelper.piveauId(), cause);
                                if (cause instanceof TripleStoreException te) {
                                    if (!te.getQuery().isBlank()) {
                                        logger.error("Query: {}", te.getQuery());
                                    }
                                }
                            });

                    if (translationService != null) {
                        return Future.<DatasetHelper>future(promise -> translationService.initializeTranslationProcess(datasetHelper, null, false, promise)).otherwise(datasetHelper);
                    } else {
                        return Future.succeededFuture(datasetHelper);
                    }
                });
    }

    private Future<DatasetHelper> updateDataset(DatasetHelper datasetHelper, String recordUriRef) {
        return datasetManager.getGraph(DCATAPUriSchema.parseUriRef(recordUriRef).getDatasetGraphName())
                .compose(model -> {
                    datasetHelper.update(model, recordUriRef);

                    if (translationService != null) {
                        return DatasetHelper.create(Piveau.presentAs(model, Lang.NTRIPLES), Lang.NTRIPLES.getContentType().getContentTypeStr())
                                .compose(oldHelper -> Future.<DatasetHelper>future(trans -> translationService.initializeTranslationProcess(datasetHelper, oldHelper, false, trans)))
                                .otherwise(datasetHelper);
                    } else {
                        return Future.succeededFuture(datasetHelper);
                    }
                })
                .recover(cause -> {
                    throw new ServiceException(500, cause.getMessage(), new JsonObject()
                            .put("recordUriRef", recordUriRef)
                            .put("datasetHelper", datasetHelper.toJson()));
                });
    }

    private Future<String> findPiveauId(String id, int counter) {
        AtomicReference<String> checkId = new AtomicReference<>();
        if (counter > 0) {
            checkId.set(id + "~~" + counter);
        } else {
            checkId.set(id);
        }
        String askQuery = sparqlQueryPool.format("askPiveauId", DCATAPUriSchema.createForDataset(checkId.get()).getUriRef());
        return tripleStore.ask(askQuery)
                .compose(exist -> {
                    if (exist) {
                        return findPiveauId(id, counter + 1);
                    } else {
                        return Future.succeededFuture(checkId.get());
                    }
                });
    }

    private Future<JsonObject> catalogueInfo(String catalogueId) {
        if (cache.containsKey(catalogueId)) {
            return Future.succeededFuture(cache.get(catalogueId));
        }

        DCATAPUriRef catalogueUriRef = DCATAPUriSchema.createForCatalogue(catalogueId);
        String query = sparqlQueryPool.format("catalogueQuery", catalogueUriRef.getGraphName(), catalogueUriRef.getUriRef());
        return tripleStore.select(query)
                .map(resultSet -> {
                    if (resultSet.hasNext()) {
                        JsonObject result = new JsonObject();
                        QuerySolution solution = resultSet.next();
                        if (solution.contains("lang")) {
                            Concept concept = Languages.INSTANCE.getConcept(solution.getResource("lang"));
                            if (concept != null) {
                                String langCode = Languages.INSTANCE.iso6391Code(concept);
                                if (langCode == null) {
                                    langCode = Languages.INSTANCE.tedCode(concept);
                                }
                                if (langCode != null) {
                                    result.put("langCode", langCode.toLowerCase());
                                }
                            }
                        }
                        if (solution.contains("type")) {
                            result.put("type", solution.getLiteral("type").getLexicalForm());
                        }
                        if (solution.contains("super")) {
                            result.put("superCatalogue", solution.getResource("super").getURI());
                        }
                        if (solution.contains("visibility")) {
                            result.put("visibility", solution.getResource("visibility").getLocalName());
                        }
                        cache.putIfAbsent(catalogueId, result);
                        return result;
                    } else {
                        throw new ServiceException(404, "Catalogue not found");
                    }
                });
    }

    private ServiceException mapCause(Throwable cause) {
        if (cause instanceof ServiceException serviceException) {
            return serviceException;
        } else if (cause instanceof DoesNotExistException doesNotExistException) {
            return new ServiceException(422, "Class expected but not found in data: " + doesNotExistException.getMessage());
        } else if (cause instanceof RiotException riotException) {
            return new ServiceException(400, "RDF parsing exception: " + riotException.getMessage());
        } else if (cause.getMessage().startsWith("Not found")) {
            return new ServiceException(404, "Dataset not found");
        } else if (cause instanceof TripleStoreException tripleStoreException) {
            return new ServiceException(500, "Triple store exception: " + tripleStoreException.getMessage(),
                    new JsonObject().put("query", tripleStoreException.getQuery()));
        } else {
            return new ServiceException(500, cause.getMessage());
        }
    }

}
