/*
 * Copyright (c) Fraunhofer FOKUS
 *
 * This program and the accompanying materials are made available under the
 * terms of the Apache License, Version 2.0 which is available at
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package io.piveau.hub.ext.trust.services;


import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.file.FileSystem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StorageService {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    FileSystem fileSystem;

    public StorageService(FileSystem fileSystem) {
        this.fileSystem = fileSystem;
    }

    public Future<Void> writeFile(String fileName, String fileContent) {
        // Sanitize the fileName by replacing ':' with '%3A'
        String sanitizedFileName = fileName.replace(":", "%3A");

        // Extract directory path
        int lastSlashIndex = sanitizedFileName.lastIndexOf("/");
        String directoryPath = (lastSlashIndex != -1) ? sanitizedFileName.substring(0, lastSlashIndex + 1) : "";
        String pureFileName = (lastSlashIndex != -1) ? sanitizedFileName.substring(lastSlashIndex + 1) : sanitizedFileName;

        // Ensure directory exists
        Future<Void> directoryFuture = ensureDirectory(directoryPath);

        // Write the file after directory is ensured
        Future<Void> writeFuture = directoryFuture.compose(v -> {
            Promise<Void> writePromise = Promise.promise();
            fileSystem.writeFile(directoryPath + pureFileName, Buffer.buffer(fileContent), result -> {
                if (result.succeeded()) {
                    writePromise.complete();
                } else {
                    writePromise.fail(result.cause());
                }
            });
            return writePromise.future();
        });

        return writeFuture;
    }


    public Future<String> readFile(String filePath) {
        Promise<String> promise = Promise.promise();

        // Sanitize the filePath by replacing ':' with '%3A'
        String sanitizedFilePath = filePath.replace(":", "%3A");

        // Asynchronously read the file content
        fileSystem.readFile(sanitizedFilePath, result -> {
            if (result.succeeded()) {
                try {
                    String file = result.result().toString();
                    promise.complete(file);
                } catch (Exception e) {
                    promise.fail(e);
                }
            } else {
                promise.fail(result.cause());
            }
        });

        return promise.future();
    }

    /**
     * Ensures that a directory exists, creating it if necessary.
     *
     * @param directoryPath the path of the directory to check/create
     * @return Future representing the result of the directory check/create operation
     */
    private Future<Void> ensureDirectory(String directoryPath) {
        Promise<Void> promise = Promise.promise();

        // Check if the directory exists
        fileSystem.exists(directoryPath, dirCheckResult -> {
            if (dirCheckResult.succeeded()) {
                if (dirCheckResult.result()) {
                    // Directory exists
                    promise.complete();
                } else {
                    // Directory does not exist, create it
                    fileSystem.mkdirs(directoryPath, mkdirResult -> {
                        if (mkdirResult.succeeded()) {
                            promise.complete();
                        } else {
                            promise.fail(mkdirResult.cause());
                        }
                    });
                }
            } else {
                promise.fail(dirCheckResult.cause());
            }
        });

        return promise.future();
    }

}
