/*
 * Copyright (c) Fraunhofer FOKUS
 *
 * This program and the accompanying materials are made available under the
 * terms of the Apache License, Version 2.0 which is available at
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package io.piveau.hub.ext.trust.services;

import com.nimbusds.jose.jwk.RSAKey;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DIDService {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    StorageService storageService;
    String storageLocation;

    public DIDService(String storageLocation, StorageService storageService) {
        this.storageLocation = storageLocation;
        this.storageService = storageService;
    }

    // Creates and stores DID Document
    public Future<JsonObject> createDID(String did, String verificationMethodType, RSAKey rsaPublicKey) throws IllegalArgumentException {
        Promise<JsonObject> promise = Promise.promise();

        // Check if verificationMethodType currently supported
        JsonArray supportedVerificationMethods = new JsonArray()
            .add("JWK2020-RSA");
        if (!supportedVerificationMethods.contains(verificationMethodType)) {
            throw new IllegalArgumentException("Verification method type '" + verificationMethodType + "' is not currently supported.");
        }

        // Create DID Document with context and id
        JsonObject didDocument = new JsonObject()
            .put("@context", new JsonArray()
                .add("https://www.w3.org/ns/did/v1") 
                .add("https://w3id.org/security/suites/jws-2020/v1"))
            .put("id", did);

        // Verification Method
        if ("JWK2020-RSA".equals(verificationMethodType)) {
            JsonObject publicKeyJwkJSON = new JsonObject(rsaPublicKey.toJSONString());

            publicKeyJwkJSON
                .put("alg", "RS256")
                .put("x5u", "https://ids.fokus.fraunhofer.de/.well-known/fullchain.pem");

            JsonObject verificationMethodObject = new JsonObject()
                .put("id", did + "#JWK2020-RSA")
                .put("type", "JsonWebKey2020")
                .put("controller", did)
                .put("publicKeyJwk", publicKeyJwkJSON);

            // Add the verificationMethod and assertionMethod to the DID document
            didDocument
                .put("verificationMethod", new JsonArray().add(verificationMethodObject))
                .put("assertionMethod", new JsonArray().add(did + "#" + verificationMethodType));
        }

        // Store DID Document
        String fileName = storageLocation + "/" + did.replace(":", "-") + ".json";
        String file = didDocument.toString();
        Future<Void> writeDid = storageService.writeFile(fileName, file);
        writeDid.onComplete(res -> {
            if (res.succeeded()) {
                promise.complete(didDocument);
            } else {
                logger.debug("Failed to write DID: {}", res.cause().getMessage());
                promise.fail(res.cause());
            }
        });

        return promise.future();
    }

    // Get DID Document
    public Future<JsonObject> getDID(String did) {
        Promise<JsonObject> promise = Promise.promise();

        String fileName = storageLocation + "/" + did.replace(":", "-") + ".json";
        storageService.readFile(fileName).onComplete(readResult -> {
            if (readResult.succeeded()) {
                String fileContent = readResult.result();
                JsonObject didDocument = new JsonObject(fileContent);
                promise.complete(didDocument);
            } else {
                logger.error("Failed to read DID file", readResult.cause());
                promise.fail(readResult.cause());
            }
        });

        return promise.future();
    }
}
