package io.piveau.hub.shell

import io.piveau.dcatap.TripleStore
import io.piveau.hub.CommandLog
import io.piveau.hub.Constants
import io.piveau.hub.jobs.InstallVocabulariesJob
import io.piveau.json.asJsonObject
import io.vertx.core.Vertx
import io.vertx.core.cli.Argument
import io.vertx.core.cli.CLI
import io.vertx.core.cli.Option
import io.vertx.ext.shell.command.Command
import io.vertx.ext.shell.command.CommandBuilder
import io.vertx.ext.shell.command.CommandProcess
import io.vertx.ext.web.client.WebClient
import io.vertx.kotlin.coroutines.coAwait

class InstallVocabulariesCommand private constructor(vertx: Vertx) {

    private val tripleStore: TripleStore

    private val command: Command

    init {
        val config = vertx.orCreateContext.config()
        tripleStore = TripleStore(
            vertx,
            config.asJsonObject(Constants.ENV_PIVEAU_TRIPLESTORE_CONFIG),
            WebClient.create(vertx)
        )

        command = CommandBuilder.command(
            CLI.create("installVocabularies")
                .setSummary("Install a selected list of vocabularies.")
                .addOption(
                    Option()
                        .setArgName("remote")
                        .setShortName("r")
                        .setLongName("remote")
                        .setDescription("If possible, fetch vocabulary from remote, otherwise load from local file.")
                        .setFlag(true)
                )
                .addOption(
                    Option()
                        .setArgName("list")
                        .setShortName("l")
                        .setLongName("list")
                        .setDescription("List installable vocabularies and their configuration.")
                        .setFlag(true)
                )
                .addOption(
                    Option()
                        .setHelp(true)
                        .setFlag(true)
                        .setArgName("help")
                        .setShortName("h")
                        .setLongName("help")
                )
                .addOption(
                    Option()
                        .setArgName("tags")
                        .setShortName("t")
                        .setLongName("tags")
                        .setDescription("Install only tagged vocabularies. If you don't specify a tag, all 'core' tagged vocabularies will be installed.")
                        .setMultiValued(true)
                        .setDefaultValue("core")
                )
                .addArgument(
                    Argument()
                        .setArgName("vocabulary")
                        .setDescription("Specific vocabulary ids")
                        .setRequired(false)
                        .setMultiValued(true)
                )
        ).scopedProcessHandler(::install).build(vertx)
    }

    private suspend fun install(process: CommandProcess) {

        val remote = process.commandLine().isFlagEnabled("remote")

        val ids = process.commandLine().getArgumentValues<String>(0)

        val tags = process.commandLine().getOptionValues<String>("tags").toMutableList()
        if (tags.isEmpty() && ids.isEmpty()) {
            tags.add("core")
        }

        val log = CommandLog(process)
        val job = InstallVocabulariesJob(process.vertx(), tripleStore, log, remote, tags, ids)

        val list = process.commandLine().isFlagEnabled("list")
        if (list) {
            job.list()
        } else {
            job.execute().coAwait()
        }
    }

    companion object {
        fun create(vertx: Vertx) = InstallVocabulariesCommand(vertx).command
    }
}
