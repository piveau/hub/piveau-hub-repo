package io.piveau.hub.shell

import io.piveau.dcatap.TripleStore
import io.piveau.hub.CommandLog
import io.piveau.hub.Constants
import io.piveau.hub.jobs.RepairJob
import io.piveau.json.asJsonObject
import io.piveau.utils.candidatesCompletion
import io.vertx.core.Vertx
import io.vertx.core.cli.Argument
import io.vertx.core.cli.CLI
import io.vertx.core.cli.Option
import io.vertx.ext.shell.command.Command
import io.vertx.ext.shell.command.CommandBuilder
import io.vertx.ext.shell.command.CommandProcess
import io.vertx.ext.web.client.WebClient
import io.vertx.kotlin.coroutines.coAwait
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch

class RepairCatalogueCommand private constructor(vertx: Vertx) {
    private val command: Command
    private val tripleStore: TripleStore
    private val catalogueManager
        get() = tripleStore.catalogueManager

    init {
        val config = vertx.orCreateContext.config()
        tripleStore = TripleStore(
            vertx,
            config.asJsonObject(Constants.ENV_PIVEAU_TRIPLESTORE_CONFIG),
            WebClient.create(vertx)
        )

        command = CommandBuilder.command(
            CLI.create("repair")
                .addArgument(
                    Argument()
                        .setArgName("catalogueId")
                        .setRequired(false)
                        .setDescription("The id of the catalogue")
                )
                .addOption(
                    Option()
                        .setArgName("offset")
                        .setLongName("offset")
                        .setShortName("o")
                        .setDefaultValue("0")
                        .setDescription("Catalogue list offset")
                        .setSingleValued(true)
                )
                .addOption(
                    Option()
                        .setArgName("limit")
                        .setLongName("limit")
                        .setShortName("l")
                        .setDefaultValue("500")
                        .setDescription("Catalogue list limit")
                        .setSingleValued(true)
                )
                .addOption(
                    Option().setHelp(true).setFlag(true).setArgName("help").setShortName("h").setLongName("help")
                        .setDescription("This help")
                )
        ).completionHandler { completion ->
            catalogueManager.listUris().onSuccess { list ->
                val ids = list.map { it.id }
                completion.candidatesCompletion(ids)
            }.onFailure { completion.complete("", true) }
        }.scopedProcessHandler(this::handleCommand).build(vertx)
    }

    private suspend fun handleCommand(process: CommandProcess) {
        val catalogues = extractCatalogueList(catalogueManager.listUris().coAwait().map { it.id }.sorted(), process)

        val log = CommandLog(process)
        coroutineScope {
            async {
                RepairJob(tripleStore, catalogues, log).execute()
            }
        }
    }

    companion object {
        fun create(vertx: Vertx): Command {
            return RepairCatalogueCommand(vertx).command
        }
    }

}
