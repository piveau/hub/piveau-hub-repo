package io.piveau.hub.shell

import io.piveau.dcatap.TripleStore
import io.piveau.hub.Constants
import io.piveau.json.asJsonObject
import io.piveau.sparql.SparqlQueryPool
import io.vertx.core.Vertx
import io.vertx.core.cli.CLI
import io.vertx.core.cli.Option
import io.vertx.ext.shell.command.Command
import io.vertx.ext.shell.command.CommandBuilder
import io.vertx.ext.web.client.WebClient
import io.vertx.kotlin.coroutines.await
import java.nio.file.Path

class DeleteHistoryCommand private constructor(vertx: Vertx) {

    private val command: Command

    private val tripleStore: TripleStore

    private val client: WebClient = WebClient.create(vertx)

    init {
        val config = vertx.orCreateContext.config()
        tripleStore = TripleStore(
            vertx,
            config.asJsonObject(Constants.ENV_PIVEAU_TRIPLESTORE_CONFIG),
            client
        )
        command = CommandBuilder.command(
            CLI.create("deleteMetricsHistories")
                .setDescription("Delete old metrics history graphs from main system")
                .addOption(
                    Option().setArgName("help")
                        .setHelp(true)
                        .setFlag(true)
                        .setShortName("h")
                        .setLongName("help")
                        .setDescription("This help")
                )
        ).scopedProcessHandler { process ->

            val query = SparqlQueryPool.create(vertx, Path.of("queries")).format("metricsHistories") ?: ""

            do {
                val resultSet = tripleStore.select(query).await()
                var cont = false
                if (resultSet.hasNext()) {
                    val histories = resultSet.asSequence().map { qr -> qr.getResource("g") }.toList()

                    histories.forEach {
                        tripleStore.deleteGraph(it.uri).await()
                        process.write("Deleted ${it.uri}\n")
                    }
                    cont = true
                }
            } while (cont)

            process.write("Flow finished\n")
        }.build(vertx)
    }

    companion object {
        fun create(vertx: Vertx): Command {
            return DeleteHistoryCommand(vertx).command
        }
    }

}
