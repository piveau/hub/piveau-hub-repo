package io.piveau.hub.shell

import io.piveau.dcatap.TripleStore
import io.piveau.hub.CommandLog
import io.piveau.hub.Constants
import io.piveau.hub.jobs.ClearCatalogueJob
import io.piveau.json.asJsonObject
import io.piveau.utils.candidatesCompletion
import io.vertx.core.*
import io.vertx.core.cli.Argument
import io.vertx.core.cli.CLI
import io.vertx.core.cli.Option
import io.vertx.ext.shell.command.Command
import io.vertx.ext.shell.command.CommandBuilder
import io.vertx.ext.shell.command.CommandProcess
import io.vertx.ext.web.client.WebClient

@kotlin.time.ExperimentalTime
class ClearCatalogueCommand private constructor(vertx: Vertx) {
    private val command: Command
    private val tripleStore: TripleStore
    private val catalogueManager
        get() = tripleStore.catalogueManager

    init {
        val config = vertx.orCreateContext.config()
        tripleStore = TripleStore(
            vertx,
            config.asJsonObject(Constants.ENV_PIVEAU_TRIPLESTORE_CONFIG),
            WebClient.create(vertx)
        )

        command = CommandBuilder.command(
            CLI.create("clear")
                .addArgument(
                    Argument()
                        .setArgName("catalogueId")
                        .setRequired(true)
                        .setDescription("The id of the catalogues to clear")
                )
                .addOption(
                    Option().setArgName("help")
                        .setHelp(true)
                        .setFlag(true)
                        .setShortName("h")
                        .setLongName("help")
                        .setDescription("This help")
                )
        ).completionHandler { completion ->
            catalogueManager.listUris().onSuccess { list ->
                val ids = list.map { it.id }.sorted()
                completion.candidatesCompletion(ids)
            }.onFailure { completion.complete("", false) }
        }.scopedProcessHandler(this::index).build(vertx)
    }

    private suspend fun index(process: CommandProcess) {
        val catalogueId = process.commandLine().getArgumentValue<String>(0)

        val log = CommandLog(process)
        val job = ClearCatalogueJob(process.vertx(), tripleStore, log, catalogueId)
        job.execute()
    }

    companion object {
        fun create(vertx: Vertx): Command {
            return ClearCatalogueCommand(vertx).command
        }
    }

}
