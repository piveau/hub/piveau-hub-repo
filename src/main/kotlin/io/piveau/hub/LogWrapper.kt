package io.piveau.hub

import io.vertx.ext.shell.command.CommandProcess
import org.jobrunr.jobs.context.JobRunrDashboardLogger
import org.slf4j.Logger
import org.slf4j.LoggerFactory

interface LogWrapper {
    fun info(pattern: String, vararg args: Any)
    fun debug(pattern: String, vararg args: Any)
    fun warn(pattern: String, vararg args: Any)
    fun error(pattern: String, vararg args: Any)
    fun trace(pattern: String, vararg args: Any)

    fun print(pattern: String, vararg args: Any)
}

class CommandLog(val command: CommandProcess) : LogWrapper {
    override fun info(pattern: String, vararg args: Any) {
        command.write("[INFO] ${pattern.format(*args)}\n")
    }

    override fun debug(pattern: String, vararg args: Any) {
        command.write("[DEBUG] ${pattern.format(*args)}\n")
    }

    override fun warn(pattern: String, vararg args: Any) {
        command.write("[WARNING] ${pattern.format(*args)}\n")
    }

    override fun error(pattern: String, vararg args: Any) {
        command.write("[ERROR] ${pattern.format(*args)}\n")
    }

    override fun trace(pattern: String, vararg args: Any) {
        command.write("[TRACE] ${pattern.format(*args)}\n")
    }

    override fun print(pattern: String, vararg args: Any) {
        command.write(pattern.format(*args))
    }

}

open class SLF4JLog(loggerName: String) : LogWrapper {

    protected open val log: Logger = LoggerFactory.getLogger(loggerName)

    override fun info(pattern: String, vararg args: Any) {
        log.info(pattern, *args)
    }

    override fun debug(pattern: String, vararg args: Any) {
        log.debug(pattern, *args)
    }

    override fun warn(pattern: String, vararg args: Any) {
        log.warn(pattern, *args)
    }

    override fun error(pattern: String, vararg args: Any) {
        log.error(pattern, *args)
    }

    override fun trace(pattern: String, vararg args: Any) {
        log.trace(pattern, *args)
    }

    override fun print(pattern: String, vararg args: Any) {
        log.info(pattern, *args)
    }

}

open class JobRunrLog(loggerName: String) : SLF4JLog(loggerName) {
    override val log = JobRunrDashboardLogger(LoggerFactory.getLogger(loggerName))
}
