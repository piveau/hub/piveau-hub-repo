package io.piveau.hub.jobs

import io.piveau.dcatap.TripleStore
import io.piveau.hub.LogWrapper
import io.piveau.hub.services.catalogues.CataloguesService
import io.piveau.hub.services.datasets.DatasetsService
import io.piveau.hub.services.vocabularies.VocabulariesService
import io.piveau.hub.vocabularies.fetchFromRemote
import io.piveau.hub.vocabularies.mergeExtensions
import io.piveau.hub.vocabularies.updateVocabularyDataset
import io.piveau.rdf.RDFMimeTypes
import io.piveau.rdf.loadToModel
import io.piveau.rdf.saveToTmpFile
import io.piveau.sparql.SparqlQueryPool.Companion.create
import io.piveau.utils.canonicalHash
import io.vertx.core.Future
import io.vertx.core.Vertx
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.client.WebClient
import io.vertx.kotlin.coroutines.coAwait
import org.apache.jena.riot.Lang
import java.nio.file.Path
import kotlin.io.path.Path
import kotlin.reflect.cast

class InstallVocabulariesJob(
    val vertx: Vertx,
    val tripleStore: TripleStore,
    log: LogWrapper,
    val remote: Boolean = false,
    val tags: List<String> = listOf("core"),
    val ids: List<String> = emptyList(),
) : Job(log) {

    val client: WebClient = WebClient.create(vertx)

    val vocabulariesService: VocabulariesService = VocabulariesService.createProxy(vertx, VocabulariesService.SERVICE_ADDRESS)
    val datasetsService: DatasetsService = DatasetsService.createProxy(vertx, DatasetsService.SERVICE_ADDRESS)
    val cataloguesService: CataloguesService = CataloguesService.createProxy(vertx, CataloguesService.SERVICE_ADDRESS)

    val sparqlQueryPool = create(vertx, Path.of("queries"))

    override suspend fun execute(): Future<JsonObject> {
        log.info("Installing vocabularies")

        return vertx.fileSystem().readFile("vocabularies.json")
            .map { it.toJsonArray() }
            .compose { vocabularies ->
                val finalList = vocabularies
                    .map { it as JsonObject }
                    .filter {
                        if (ids.isNotEmpty()) {
                            ids.contains(it.getString("id"))
                        } else {
                            val vocabularyTags =
                                it.getJsonArray("tags", JsonArray()).map(String::class::cast).toSet()
                            tags.any { tag -> vocabularyTags.contains(tag) }
                        }
                    }
                installNext(finalList.iterator())
            }
            .onSuccess { log.info("Vocabularies installed") }
    }

    override suspend fun status(): JsonObject = JsonObject()

    suspend fun list() {
        val vocabularies = vertx.fileSystem().readFile("vocabularies.json").coAwait().toJsonArray()
        log.print("${vocabularies.encodePrettily()}\n")
    }

    private fun installNext(vocabularies: Iterator<JsonObject>): Future<JsonObject> {
        return if (vocabularies.hasNext()) {
            installVocabulary(vocabularies.next())
                .compose { installNext(vocabularies) }
        } else {
            Future.succeededFuture(JsonObject())
        }
    }

    private fun installVocabulary(vocabulary: JsonObject): Future<JsonObject> {
        val id = vocabulary.getString("id")
        val uriRef = vocabulary.getString("uriRef")

        log.info("Installing $id ($uriRef)...")

        val future = when {
            remote && vocabulary.containsKey("remote") -> {
                fetchFromRemote(
                    vertx,
                    client,
                    vocabulary.getJsonObject("remote", JsonObject())
                )
            }

            vocabulary.containsKey("file") -> Future.succeededFuture(vocabulary.getString("file"))

            else -> {
                log.error("Vocabulary $id is not properly configured")
                Future.failedFuture("Vocabulary $id is not properly configured")
            }
        }

        return future
            .compose { filename -> Path(filename).loadToModel(vertx) }
            .compose { model -> mergeExtensions(model, vocabulary, vertx) }
            .compose { model ->
                val query = sparqlQueryPool.format("vocabularyHash", uriRef) as String
                tripleStore.select(query).map { model to it }
            }
            .compose { (model, resultSet) ->
                // check hash value
                val oldHash = if (resultSet.hasNext()) {
                    resultSet.next().getLiteral("hash").lexicalForm
                } else {
                    ""
                }
                val newHash = model.canonicalHash()
                if (oldHash.isEmpty() || newHash != oldHash) {
                    model.saveToTmpFile(Lang.NTRIPLES, vertx).map { it to newHash }
                } else {
                    Future.succeededFuture("" to oldHash)
                }
            }
            .compose { (tmpFilename, hash) ->
                if (tmpFilename.isNotEmpty()) {
                    vocabulariesService.putVocabularyFile(
                        id,
                        uriRef,
                        RDFMimeTypes.NTRIPLES,
                        tmpFilename
                    ).map { it to hash }
                } else {
                    Future.succeededFuture("skipped" to "")
                }
            }
            .compose { (status, hash) ->
                if (status != "skipped") {
                    updateVocabularyDataset(datasetsService, cataloguesService, uriRef, id, hash)
                } else {
                    Future.succeededFuture(JsonObject().put("status", status))
                }
            }
            .otherwise(JsonObject().put("status", "failed"))
            .onSuccess { log.info("Vocabulary $id ${it.getString("status")}") }
    }

}