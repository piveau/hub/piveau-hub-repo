package io.piveau.hub.jobs

import io.piveau.dcatap.TripleStore
import io.piveau.hub.Constants
import io.piveau.hub.JobRunrLog
import io.piveau.hub.SLF4JLog
import io.piveau.json.asJsonObject
import io.piveau.utils.FriendlyWords
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.client.WebClient
import io.vertx.kotlin.coroutines.CoroutineEventBusSupport
import io.vertx.kotlin.coroutines.CoroutineVerticle
import kotlin.reflect.cast

class JobVerticle : CoroutineVerticle(), CoroutineEventBusSupport {

    override suspend fun start() {
        val config = vertx.orCreateContext.config()
        val tripleStore = TripleStore(
            vertx,
            config.asJsonObject(Constants.ENV_PIVEAU_TRIPLESTORE_CONFIG),
            WebClient.create(vertx)
        )

        vertx.eventBus().coConsumer<JsonObject>(ADDRESS) { message ->
            val action = message.headers()["action"] ?: ""
            val params = message.body()

            val jobName = message.headers()["scheduler-job-name"] ?: FriendlyWords().generateOutOfTwo()

            val job = when (action) {
                "buildIndex" -> IndexJob(
                    vertx,
                    tripleStore,
                    SLF4JLog("$jobName-buildIndex"),
                    params.getJsonArray("catalogues").map(String::class::cast),
                    params.getBoolean("deep", false)
                )

                "installVocabularies" -> InstallVocabulariesJob(
                    vertx,
                    tripleStore,
                    JobRunrLog("$jobName-installVocabularies"),
                    remote = params.getBoolean("remote"),
                    ids = params.getJsonArray("ids", JsonArray()).map(String::class::cast),
                    tags = params.getJsonArray("tags", JsonArray()).map(String::class::cast)
                )
//                "repair" -> RepairJob(
//                    ,
//                    params.getJsonArray("catalogues").map(String::class::cast),
//                    SLF4JLog("$jobName-repair")
//                )
                else -> VoidJob()
            }

            job.execute()
                .onSuccess { message.reply(it) }
                .onFailure { message.fail(-2, it.message) }
        }
    }

    companion object {
        const val ADDRESS = "io.piveau.hub.job.service"
    }

}