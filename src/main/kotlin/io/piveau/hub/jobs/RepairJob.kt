package io.piveau.hub.jobs

import io.piveau.dcatap.DCATAPUriRef
import io.piveau.dcatap.DCATAPUriSchema
import io.piveau.dcatap.TripleStore
import io.piveau.hub.LogWrapper
import io.vertx.core.Future
import io.vertx.core.json.JsonObject
import io.vertx.kotlin.coroutines.coAwait
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.toSet
import kotlinx.coroutines.flow.transform
import org.apache.jena.vocabulary.DCAT

class RepairJob(val tripleStore: TripleStore, val catalogues: List<String>, log: LogWrapper) : Job(log) {

    private val catalogueManager = tripleStore.catalogueManager
    private val datasetManager = tripleStore.datasetManager

    override suspend fun execute(): Future<JsonObject> {
        catalogues.forEach { repairCatalogue(it, catalogues.size == 1) }
        return Future.succeededFuture(JsonObject())
    }

    override suspend fun status(): JsonObject = JsonObject()

    private suspend fun repairCatalogue(catalogueId: String, travers: Boolean) {
        val catalogues = mutableListOf(DCATAPUriSchema.createForCatalogue(catalogueId))

        if (travers) {
            val subCatalogues = catalogueManager.subCatalogues(catalogueId).coAwait()
            if (subCatalogues.isNotEmpty()) {
                log.info("Found ${subCatalogues.size} sub-catalogues")
            }
            catalogues.addAll(subCatalogues)
        }

        catalogues
            .forEach { catalogue ->
                val validRecords = checkDatasets(catalogue)
                removeInvalidRecordEntries(catalogue, validRecords)
            }
    }

    private suspend fun checkDatasets(catalogueUriRef: DCATAPUriRef): Set<String> {
        val datasets = catalogueManager.allDatasets(catalogueUriRef.id).coAwait().toSet()
        log.info("Found ${datasets.size} dataset entries in catalogue ${catalogueUriRef.id}")
        return datasets
            .asFlow()
            .transform {
                try {
                    if (datasetManager.exist(it).coAwait()) {
                        emit(it)
                    } else {
                        log.warn("Dataset ${it.id} does not really exist. Removing entry from catalogue")
                        datasetManager.deleteGraph(it.graphNameRef)
                        catalogueManager.removeDatasetEntry(catalogueUriRef.uriRef, it.uriRef)
                    }
                } catch (e: Exception) {
                    log.error("Dataset ${it.id} exists check failure: ${e.message}")
                }
            }
            .transform { datasetUriRef ->
                try {
                    if (!tripleStore.ask(
                            ASK_TEMPLATE.format(
                                catalogueUriRef.graphName,
                                catalogueUriRef.uriRef,
                                datasetUriRef.uriRef,
                                datasetUriRef.recordUriRef
                            )
                        ).coAwait()
                    ) {
                        catalogueManager
                            .addDatasetEntry(
                                catalogueUriRef.graphName,
                                catalogueUriRef.uriRef,
                                datasetUriRef.uriRef,
                                datasetUriRef.recordUriRef
                            ).coAwait()
                    }

                    emit(datasetUriRef.recordUriRef)
                } catch (e: Exception) {
                    log.error("Dataset ${datasetUriRef.id} refresh failure: ${e.message}")
                }
            }.toSet()
    }

    private suspend fun removeInvalidRecordEntries(catalogueUriRef: DCATAPUriRef, validRecords: Set<String>) {
        val allRecords = catalogueManager.allRecords(catalogueUriRef.id).coAwait()
            .map { it.recordUriRef }
            .toSet()
        log.info("Found ${allRecords.size} record entries in catalogue ${catalogueUriRef.id}")

        (allRecords - validRecords)
            .forEach {
                try {
                    tripleStore.update(
                        "DELETE DATA { GRAPH <${catalogueUriRef.graphNameRef}> { <${catalogueUriRef.uriRef}> <${DCAT.record}> <$it> } }"
                    ).coAwait()
                } catch (e: Exception) {
                    log.error("Removing record entry for $it: ${e.message}")
                }
            }
    }

    companion object {
        private const val ASK_TEMPLATE = "ASK { GRAPH <%s> { <%s> <http://www.w3.ord/ns/dcat#dataset> <%s> ; <http://www.w3.ord/ns/dcat#record> <%s> } }"
    }

}
