package io.piveau.hub

import io.piveau.hub.jobs.JobVerticle
import io.piveau.utils.FriendlyWords
import io.vertx.core.Vertx
import io.vertx.core.eventbus.DeliveryOptions
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.kotlin.coroutines.coAwait
import kotlinx.coroutines.runBlocking
import org.jobrunr.configuration.JobRunr
import org.jobrunr.jobs.context.JobRunrDashboardLogger
import org.jobrunr.jobs.lambdas.JobRequest
import org.jobrunr.jobs.lambdas.JobRequestHandler
import org.jobrunr.scheduling.JobRequestScheduler
import org.jobrunr.scheduling.RecurringJobBuilder
import org.jobrunr.storage.InMemoryStorageProvider
import org.slf4j.LoggerFactory

object RepoScheduler {

    private val log = LoggerFactory.getLogger(RepoScheduler::class.java)

    private lateinit var jobRequestScheduler: JobRequestScheduler

    lateinit var vertx: Vertx

    private val friendlyWords = FriendlyWords()

    @JvmStatic
    fun config(vertx: Vertx, config: JsonObject) {
        this.vertx = vertx

        jobRequestScheduler = JobRunr
            .configure()
            .useStorageProvider(InMemoryStorageProvider())
            .useBackgroundJobServer()
            .useDashboardIf(
                config.getBoolean("useDashboard", false),
                config.getInteger("dashboardPort", 8000),
            )
            .initialize()
            .jobRequestScheduler

        val configuredJobs = config.getJsonArray("jobs", JsonArray())
        configuredJobs.stream()
            .filter { it is JsonObject }
            .map { it as JsonObject }
            .filter { it.containsKey("job") && it.containsKey("name") && it.getString("cron", "").isNotEmpty() }
            .forEach { jobConfig ->
                val job = jobConfig.getString("job")
                val name = jobConfig.getString("name")
                val cron = jobConfig.getString("cron")
                val timeoutSec = jobConfig.getLong("timeoutSec", 300)
                val params = jobConfig.getJsonObject("params", JsonObject())
                scheduleRecurrently(cron, RepoJobRequest(job, name, params, timeoutSec))
            }
    }

    @JvmStatic
    fun scheduleRecurrently(cron: String, job: RepoJobRequest) {
        if (this::jobRequestScheduler.isInitialized) {
            jobRequestScheduler.createRecurrently(
                RecurringJobBuilder.aRecurringJob()
                    .withId(friendlyWords.generateOutOfTwo())
                    .withName(job.name)
                    .withAmountOfRetries(0)
                    .withCron(cron)
                    .withJobRequest(job)
            )
        } else {
            log.error("Scheduler called before being configured")
        }
    }

}

class RepoJobRequestHandler : JobRequestHandler<RepoJobRequest> {
    private val log = JobRunrDashboardLogger(LoggerFactory.getLogger(this::class.qualifiedName))

    override fun run(jobRequest: RepoJobRequest): Unit = runBlocking {
        if (jobRequest.timeout == 0L) {
            RepoScheduler.vertx
                .eventBus()
                .send(JobVerticle.ADDRESS, jobRequest.params)
        } else {
            RepoScheduler.vertx
                .eventBus()
                .request<JsonObject>(
                    JobVerticle.ADDRESS,
                    jobRequest.params,
                    DeliveryOptions()
                        .setSendTimeout(jobRequest.timeout * 1000)
                        .addHeader("action", jobRequest.job)
                        .addHeader("scheduler-job-name", jobRequest.name)
                )
                .onFailure { log.error("Job runner", it) }
                .onSuccess { log.info(it.body().encodePrettily()) }
                .coAwait()
        }
    }
}

class RepoJobRequest @JvmOverloads constructor(val job: String, val name: String, val params: JsonObject, val timeout: Long = 300) : JobRequest {
    override fun getJobRequestHandler(): Class<out JobRequestHandler<RepoJobRequest>> =
        RepoJobRequestHandler::class.java
}
