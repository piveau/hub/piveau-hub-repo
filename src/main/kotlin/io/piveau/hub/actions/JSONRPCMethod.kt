package io.piveau.hub.actions

import io.vertx.core.Future
import io.vertx.core.Vertx
import io.vertx.core.json.JsonObject

abstract class JSONRPCMethod(val name: String) {
    abstract fun execute(vertx: Vertx, parameters: JsonObject): Future<JsonObject>
}

