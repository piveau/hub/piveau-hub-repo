package io.piveau.hub.actions

import io.vertx.core.Future
import io.vertx.core.Vertx
import io.vertx.core.json.JsonObject

class JSONRPCDispatcher private constructor(private val vertx: Vertx) {

    private val methodsRegister = mutableMapOf<String, JSONRPCMethod>()

    init {
        registerMethods(
            BuildIndexAction(),
            InstallVocabulariesAction()
        )
    }

    fun dispatch(request: JsonObject): Future<JsonObject> {
        val method = request.getString("method", "")

        return if (methodsRegister.containsKey(method)) {
            try {
                methodsRegister[method]
                    ?.execute(vertx, request.getJsonObject("params", JsonObject()))
                    ?: Future.failedFuture("Will never happen")
            } catch (e: JSONRPCException) {
                Future.succeededFuture(
                    JsonObject()
                        .put(
                            "error", JsonObject()
                                .put("code", JSONRPCError.InternalError.code)
                                .put("message", e.message)
                        )
                )
            }
        } else {
            Future.succeededFuture(
                JsonObject()
                    .put(
                        "error", JsonObject()
                            .put("code", JSONRPCError.MethodNotFound.code)
                            .put("message", JSONRPCError.MethodNotFound.message)
                    )
            )
        }
    }

    private fun registerMethods(vararg methods: JSONRPCMethod) {
        for (method in methods) {
            methodsRegister[method.name] = method
        }
    }

    companion object {

        @JvmStatic
        fun create(vertx: Vertx) = JSONRPCDispatcher(vertx)

    }

}