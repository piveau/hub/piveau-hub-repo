@file:JvmName("HubRepo")

package io.piveau

import io.piveau.dcatap.TripleStoreException
import io.piveau.rdf.RDFMimeTypes
import io.piveau.rdf.extractAsModel
import io.piveau.rdf.extractDatasetAsModel
import io.vertx.ext.web.RoutingContext
import io.vertx.serviceproxy.ServiceException
import org.apache.jena.rdf.model.Model
import org.apache.jena.vocabulary.DCAT
import org.apache.jena.vocabulary.RDF

fun failureResponse(routingContext: RoutingContext, cause: Throwable) = when (cause) {
    is ServiceException -> routingContext.fail(cause.failureCode(), cause.cause ?: cause)
    is TripleStoreException -> routingContext.fail(cause.code, cause.cause ?: cause)
    else -> routingContext.fail(cause)
}

// Useful for junit tests
enum class RDFMimeTypesEnum(val value: String) {
    JSONLD(RDFMimeTypes.JSONLD),
    RDFXML(RDFMimeTypes.RDFXML),
    NTRIPLES(RDFMimeTypes.NTRIPLES),
    NQUADS(RDFMimeTypes.NQUADS),
    TRIG(RDFMimeTypes.TRIG),
    TRIX(RDFMimeTypes.TRIX),
    TURTLE(RDFMimeTypes.TURTLE),
    N3(RDFMimeTypes.N3)
}

enum class ValueTypesEnum {
    uriRefs, identifiers, originalIds, metadata
}

fun Model.extractDatasetModel(): Model? {
    val dataset = listSubjectsWithProperty(RDF.type, DCAT.Dataset).toList()
    return dataset.first()?.extractDatasetAsModel()
}