@file:JvmName("HubRepo")

package io.piveau

import io.piveau.dcatap.TripleStoreException
import io.piveau.rdf.RDFMimeTypes
import io.piveau.rdf.extractDatasetAsModel
import io.piveau.vocabularies.vocabulary.LOCN
import io.vertx.core.Future
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.RoutingContext
import io.vertx.serviceproxy.ServiceException
import org.apache.jena.graph.NodeFactory
import org.apache.jena.rdf.model.Model
import org.apache.jena.rdf.model.RDFNode
import org.apache.jena.rdf.model.ResourceFactory
import org.apache.jena.rdf.model.Statement
import org.apache.jena.riot.RiotException
import org.apache.jena.riot.RiotParseException
import org.apache.jena.shared.DoesNotExistException
import org.apache.jena.vocabulary.DCAT
import org.apache.jena.vocabulary.RDF

fun failureResponse(routingContext: RoutingContext, cause: Throwable) {
    val final = cause.cause ?: cause

    when (final) {
        is ServiceException -> routingContext.fail(final.failureCode(), final)
        is TripleStoreException -> routingContext.fail(final.code, final)
        else -> routingContext.fail(final)
    }
}

fun <T> failureReply(cause: Throwable): Future<T> {
    return when(cause) {
        is ServiceException -> Future.failedFuture(cause)
        is TripleStoreException -> {
            val debug = JsonObject()
            if (cause.query.isNotBlank()) { debug.put("query", cause.query) }
            Future.failedFuture(ServiceException(cause.code, cause.message, debug))
        }
        is RiotParseException -> {
            val debug = JsonObject()
                .put("reason", cause.message)
                .put("original", cause.originalMessage)
                .put("line", cause.line)
                .put("column", cause.col)
            Future.failedFuture(ServiceException(400, "RDF Parsing Exception", debug))
        }
        is RiotException -> {
            val debug = JsonObject().put("reason", cause.message)
            Future.failedFuture(ServiceException(400, "RDF Exception", debug))
        }
        is DoesNotExistException -> Future.failedFuture(ServiceException(400, "Class expected but not found in graph: ${cause.message}"))
        else -> Future.failedFuture(ServiceException(500, cause.message))
    }
}

fun fixGeoDatatypes(model: Model) {
    val toAdd = mutableListOf<Statement>();
    val toRemove = mutableListOf<Statement>();

    model.listStatements(null, LOCN.geometry, null as RDFNode?)
        .filterKeep { stmt ->
            stmt.getObject().isLiteral && "http://www.openlinksw.com/schemas/virtrdf#Geometry" == stmt.literal.datatypeURI
        }
        .forEach { stmt ->
            toAdd.add(model.createStatement(
                stmt.subject,
                stmt.predicate,
                ResourceFactory.createTypedLiteral(
                    stmt.literal.string,
                    NodeFactory.getType("http://www.opengis.net/ont/geosparql#wktLiteral")
                )
            ))
            toRemove.add(stmt)
        }

    model.add(toAdd)
    model.remove(toRemove)
}

// Useful for junit tests
enum class RDFMimeTypesEnum(val value: String, val charset: String) {
    JSONLD(RDFMimeTypes.JSONLD, "UTF-8"),
    RDFXML(RDFMimeTypes.RDFXML, "UTF-8"),
    NTRIPLES(RDFMimeTypes.NTRIPLES, "UTF-8"),
    NQUADS(RDFMimeTypes.NQUADS, "UTF-8"),
    TRIG(RDFMimeTypes.TRIG, "UTF-8"),
    TRIX(RDFMimeTypes.TRIX, "UTF-8"),
    TURTLE(RDFMimeTypes.TURTLE, "UTF-8"),
    N3(RDFMimeTypes.N3, "UTF-8")
}

enum class ValueTypesEnum {
    uriRefs, identifiers, originalIds, metadata
}

fun Model.extractDatasetModel(): Model? {
    val dataset = listSubjectsWithProperty(RDF.type, DCAT.Dataset).toList()
    return dataset.first()?.extractDatasetAsModel()
}

object FeatureFlags {

    @JvmStatic
    var config: JsonObject = JsonObject()

    @JvmStatic
    fun isEnabled(flag: String): Boolean = config.getBoolean(flag, false)

    @JvmStatic
    fun isSet(flag: String): Boolean = config.containsKey(flag)
}

