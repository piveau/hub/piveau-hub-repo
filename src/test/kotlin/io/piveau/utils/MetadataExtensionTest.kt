package io.piveau.utils

import io.piveau.rdf.presentAsTurtle
import io.piveau.rdf.toModel
import io.vertx.core.Vertx
import io.vertx.junit5.VertxExtension
import io.vertx.junit5.VertxTestContext
import org.apache.jena.riot.Lang
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith

@DisplayName("Testing Metadata Extension")
@ExtendWith(VertxExtension::class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class MetadataExtensionTest {

    @Test
    fun `Testing extraction and update`(vertx: Vertx, testContext: VertxTestContext) {
        val modelOne = """
            prefix dext: <http://piveau.eu/ns/ext#>
            prefix dct: <http:purl.org/dc/terms/>

            <urn:test> dext:metadataExtension [
                dct:isPartOf <urn:whatever>                
            ]
        """.trimIndent().toByteArray().toModel(Lang.TURTLE)

        val modelTwo = """
            prefix dext: <http://piveau.eu/ns/ext#>
            prefix dct: <http:purl.org/dc/terms/>

            <urn:test> dext:metadataExtension [
                dct:isPartOf <urn:sowhat>                                
            ]            
        """.trimIndent().toByteArray().toModel(Lang.TURTLE)

        println(modelOne.presentAsTurtle())

        testContext.completeNow()
    }

}