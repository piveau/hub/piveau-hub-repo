# hub-repo Vocabularies Management 

The hub-repo is able to install and index almost all DCAT-AP (and some profiles) relevant vocabularies. Therefore, 
the repo manages a specific hidden catalogue `vocabularies` containing metadata about each installed vocabulary. The
distributions `accessURL` property points to the actual vocabulary which is installed in the triple store in a graph 
with the concept scheme's uriRef. At the same time each vocabulary is indexed in the search service.

The dublin core property `modified` of the vocabulary metadata indicates the last update time.

You can access the vocabularies metadata using the API or SPARQL.

## Hash value

The metadata about a vocabulary contains a canonical hash value. This helps to avoid unnecessary update of the vocabulary
and specifically the corresponding index information. Because of the nature of Elasticsearch an update of the vocabulary 
requires a very costly update of all dataset and catalogue documents.

For now and for simplicity, the value is stored in the SPDX `checksumValue` property without a `Checksum` resource.

## List of vocabularies

All available vocabularies are configured in JSON array provided in a resource json file [vocabularies.json](src/main/resources/vocabularies.json)

```json
{
  "id": "licence",
  "uriRef": "http://publications.europa.eu/resource/authority/licence",
  "file": "licences-skos.rdf",
  "remote": {
    "address": "http://publications.europa.eu/resource/distribution/licence/rdf/skos_ap_act/licences-skos-ap-act.rdf"
  },
  "extensions": [
    "piveau-licences-skos.rdf"
  ],
  "tags": [
    "core",
    "authority-tables",
    "skos"
  ]
}
```

* `id`: A unique id to handle the vocabulary
* `uriRef`: Explicit noted uriRef for the vocabulary graph
* `file`: Local resource file name
* `remote`: A remote location for downloading latest state of the vocabulary. `address`, `mimetype` (optional)
* `extensions`: Enrich the vocabulary with additional extensions from local resources
* `tags`: Assign a list of tags to this vocabulary. This allows the selection of groups of vocabularies

## Job for installing and updating vocabularies

See [hub-repo Job Management](JOBS.md). The job name is `installVocabularies`.

### Parameters

* `remote`
* `ids`
* `tags`

## Shell command

* `installVocabularies` on the command line

## Action via API

* `intallVocabularies` action

## Scheduling

You can configure the internal scheduler to run the job periodically. See [hub-repo Scheduler](JOBS.md#scheduler)

## Known issues

* Send timeout for the job verticle

  Each job ist started with a timeout. You can configure the timeout in seconds if you are able to estimate the expected 
  job's execution duration.

* Logging