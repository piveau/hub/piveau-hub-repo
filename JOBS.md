# hub-repo Jobs

Currently implemented jobs:

* Installing and updating vocabularies. See [hub-repo Vocabularies Management](VOCABULARIES.md)
* Clearing a catalogue without deleting it
* Indexing catalogue(s)
* Repairing catalogue(s) (check for identifier duplicates and "dead" references)

Upcoming jobs:

* Cleanup "zombie" datasets
* Launching metric pipe for catalogue(s)

## Scheduler

### Configuration

Currently, scheduling configuration is provided via a JSON object. The only way to pass it to the hub-repo is using an
environment variable named `PIVEAU_HUB_REPO_SCHEDULER_CONFIG`.

Example:

```json
{
  "useDashboard": true,
  "dashboardPort": 8000,
  "jobs": [
    {
      "job": "installVocabularies",
      "cron": "0 */2 * * *",
      "timeoutSec": 300,
      "params": {
        "tags": [
          "core"
        ],
        "remote": true
      }
    }
  ]
}
```

If you set the timeoutSec to `0`, the job is started without waiting for it to finish.

### Dashboard

Default port is 8000. If you change it, be aware that the official Docker images exporting only this.
